# What is this?
This repository tries to make all the challenges for OCTP available, in a single repository.
This is build using nix, and functions in the same way as [nixpkgs](https://github.com/NixOS/nixpkgs), where the source will not reside in the repository.
Instead it will be pulled in using nix (and potentially patched) and build using the set of instructions.

Generally there are two directories available:

- `challenges/` - holds all `.nix` files which describes challenges
- `lib/` - contains the various modules/tools for building the challenges

# How to build challenges?
Challenges can be build using e.g.

```
nix-build -A aarhusctf2019.checklist.tested
nix-build -A aarhusctf2019.checklist.result
```

The `tested` will try to run the tests for the specific challenge, to ensure if it works.
If no test is specified, it will just build the challenge.
However, keep in mind, that the tested result might be different from the result, as some tests might need to patch the source for running the tests.
Therefore, it is the `result` that should be used, in any production environment (ie. when pushing it to a challenge server such as https://play.deviosec.dk).

# Any questions?
Feel free to join IRC and ask in there.

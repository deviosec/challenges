{ config, extraConfig ? {} }: let
  nixpkgs = import ./nixpkgs.nix;

  extendLib = lib: let
    libOverlays = [
      (import ./lib/dag.nix)
      (import ./lib/challenge.nix { pkgs = import nixpkgs {}; })
      (import ./lib/helpers.nix { pkgs = import nixpkgs {}; })
    ];
    libOverlay = lib.foldl' lib.composeExtensions (self: super: {}) libOverlays;
  in lib.extend libOverlay;

  testpkgs = import nixpkgs {
    config = {};
    overlays = [
      (self: super: {
        lib = extendLib super.lib;
      })
    ];
    system = builtins.currentSystem;
  };

  result = testpkgs.lib.evalModules {
    modules = [
      ./modules

      config

      extraConfig

      # nixus overrides the args here, lets just
      # keep this default for now.
      {
        _module.args = {
          pkgs = testpkgs;
          inherit extendLib;
        };
      }
    ];
  };
in result

{ pkgs }:

with pkgs;

self: super: with self; {
  mkDist = name: dir: filter: let
    directoryContents = builtins.readDir dir;
    filteredDir = lib.filterAttrs filter directoryContents;

    cpCMDs = lib.concatStringsSep "\n" (lib.mapAttrsToList
      (n: _: "cp -r ${dir}/${n} ${n}")
      filteredDir
    );
  in pkgs.stdenvNoCC.mkDerivation {
    name = "${name}.zip";
    phases = "buildPhase";

    buildPhase = ''
      # create tmp folder for
      # creating zip file
      cd $(mktemp -d)
      ${cpCMDs}
      ${pkgs.zip}/bin/zip -r ${name}.zip .
      cp ${name}.zip $out
      ls -al $out
    '';
  };
}

{ pkgs }:

# TODO(eyJhb): loading config will never eval it according to the modules
# we need another way of loading/saving based on file/not file, which does not
# involve the apply method in the modules, which is generally UGLY!

with pkgs;

self: super: with self; {
  challengeModule = import ./../modules/challenge-module.nix { lib = self; };

  evalChallenge = config: pkgs.lib.evalModules { modules = [ challengeModule config ]; };

  # write config to file
  writeChallengeConfig = config: let
    evaluatedConfig = (evalChallenge config).config;
    exportedConfig = exportConfig evaluatedConfig;
    configAsYaml = jsonToYaml (builtins.toJSON exportedConfig);
  in pkgs.writeText "config.json" configAsYaml ;

  # load config from file
  loadChallengeConfig = filepath: let
    configContent = builtins.readFile filepath;
    configAsJson = builtins.fromJSON (yamlToJson configContent);
  in importConfig configAsJson;

  # helper import and exporters
  exportConfig = config: let
    # helpers for virtuals
    newPorts = ports: lib.mapAttrs (n: v: { guest = v.port; protocol = v.protocol; }) ports;
    newEnvs = envs: lib.mapAttrsToList (n: v: "${n}=${v}") envs;

    # general
    newFlags = lib.attrValues config.flags;
    newTags = lib.listToAttrs (lib.forEach config.tags (v: lib.nameValuePair v { tag = v; }));
    newVirtuals = lib.forEach (config.lab.virtuals or []) (v: v // {
      envs = newEnvs v.envs;
      ports = newPorts v.ports;
    });
    newLab = {
      name = (config.lab.names or "");
      networks = (config.lab.networks or []);
      virtuals = newVirtuals;

    };
  in config // {
    flags = newFlags;
    tags = newTags;
    lab = newLab;
  };

  importConfig = config: let
    # helpers for virtuals
    newPorts = ports: lib.mapAttrs (n: v: { port = v.guest; protocol = v.protocol or "tcp"; }) ports;
    newEnvs = let
      # split the string on =, use first element as name, remove the first and then concat them together
      newEnv = env: let
        splitEnv = lib.splitString "=" env;
      in {
        name = "${lib.elemAt splitEnv 0}";
        value = (lib.concatStringsSep "=" (lib.drop 1 splitEnv));
      };
    in envs: lib.listToAttrs (lib.forEach envs (v: (newEnv v)));

    # general
    newFlags = lib.listToAttrs (lib.forEach config.flags (v: lib.nameValuePair v.name v));
    newTags = if config ? tags then lib.mapAttrsToList (n: v: n) config.tags else [];
    newVirtuals = lib.forEach (config.lab.virtuals or []) (v: v // {
      envs = newEnvs v.envs;
      ports = newPorts v.ports;
    });

  in config // {
    flags = newFlags;
    tags = newTags;
    lab.virtuals = newVirtuals;
  };

  # helpers
  jsonToYaml = json: let
    jsonFile = pkgs.writeTextFile {
      name = "json-to-yaml";
      text = json;
    };
  in builtins.readFile (pkgs.runCommand "json-to-yaml-command" { buildInputs = with pkgs; [ jq yq ]; } ''
    yq -y . ${jsonFile} > $out
  '');

  yamlToJson = yaml: let
    yamlFile = pkgs.writeTextFile {
      name = "yaml-to-json";
      text = yaml;
    };
  in builtins.readFile (pkgs.runCommand "yaml-to-json-command" { buildInputs = with pkgs; [ jq yq ];  } ''
    yq . ${yamlFile} > $out
  '');
}

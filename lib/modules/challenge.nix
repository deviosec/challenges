{ config, pkgs, lib, ... }:

let
  inherit (lib) types;

in {
  options.challenge = {
    config = lib.mkOption {
      type = types.nullOr (types.submodule (import ./challenge-module.nix { lib = lib; }));
      default = null; 
      description = ''
        configuration for the challenge.

        If no configuration is specified, it will
        try to load `challenge.yml` from `src`.
      '';

      # if no config is set, then we try to find `challenge.yml` in the src
      apply = x: let
        configPath = "${config.src}/challenge.yml";
        pathExists = builtins.pathExists configPath;
        fileConfig = if pathExists then lib.loadChallengeConfig configPath else {};

        tmpConfig = if x == null then fileConfig else x;

        finalConfig = if tmpConfig ? tags
                        then tmpConfig // { tags = [ tmpConfig.category ] ++ tmpConfig.tags; }
                        else tmpConfig // { tags  = [ tmpConfig.category ]; };
        in finalConfig;
    };
  };
}

{ config, pkgs, lib, ... }:


let
  cfg = config.build;
  inherit (lib) types;

  # generate final challenge config
  challengeConfig = lib.recursiveUpdate config.challenge.config (config.overrides.challenge config.challenge.config);

  outputOpts = {
    options = {
      paths = lib.mkOption {
        type = types.attrsOf types.path;
        default = [];
      };
    };
  };
in {
  options.build = {
    output = lib.mkOption {
      type = types.submodule outputOpts;
      default = { paths = {}; };
    };

    extraCmds = lib.mkOption {
      type = types.lines;
      default = "";
      description = "Extra commands that should be run at the end of building the challenge.";
    };

    # just builds
    basicresult = let
      virtualsSymlink = let
        symlinks = builtins.concatStringsSep "\n" (builtins.attrValues (builtins.mapAttrs (n: v: ''
          tar --to-stdout -xf ${v} manifest.json | ${pkgs.jq}/bin/jq -r '.[0].Config' | cut -b 1-64 | tr -d "\n" > $out/virtuals/${n}.imageid
          ln -s ${v} $out/virtuals/${n}.tar.gz
        '') config.images));
      in ''
        ${if symlinks != "" then "mkdir -p $out/virtuals" else ""}
        ${symlinks}
      '';

      cpOutputPaths = lib.concatStringsSep "\n" (lib.mapAttrsToList (n: v: "ln -s ${v} $out/${n}") cfg.output.paths);

      challenge = pkgs.stdenv.mkDerivation {
        name = config.name;
        phases = [ "buildPhase" ];
        buildPhase = let
          cpSrc = if config.src == null
            then ""
            else "cp -a ${config.src}/. $out && chmod 700 $out";
        in ''
          set -xeu
          mkdir $out

          # cp src if defined
          ${cpSrc}
          
          # remove old challenge files (we have both .txt and .yml)
          # .yml is for the patched challenges (only a few)
          rm -f $out/Challenge.txt $out/challenge.yml
          cp ${lib.writeChallengeConfig challengeConfig} $out/challenge.yml
    
          # virtuals
          ${virtualsSymlink}

          # copy any outputs as well
          ${cpOutputPaths}
    
          # any extra commands to run (from inside $out)
          cd $out
          ${cfg.extraCmds}
        '';
      };
    in lib.mkOption {
      type = types.package;
      readOnly = true;
      default =  challenge;
    };

    # builds and tests
    result = let
      challengeUntested = pkgs.stdenv.mkDerivation {
        name = config.name;
        phases = [ "buildPhase" ];
        buildPhase = ''
          ln -s ${cfg.basicresult} $out
        '';
          # echo ${config.testing.test {}}
      };
    in lib.mkOption {
      type = types.package;
      readOnly = true;
      default = challengeUntested;
    };

    resulttested = let
      challengeTested = pkgs.stdenv.mkDerivation {
        name = config.name;
        phases = [ "buildPhase" ];
        buildPhase = ''
          echo ${config.testing.test {}}
          ln -s ${cfg.basicresult} $out
        '';
      };
    in lib.mkOption {
      type = types.package;
      readOnly = true;
      default = challengeTested;
    };

  };
}

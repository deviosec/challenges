{ config, pkgs, lib, ... }:

let
  cfg = config.testing;

  inherit (lib) types;

  challengeConfig = lib.recursiveUpdate config.challenge.config (config.overrides.challenge config.challenge.config);

  challengeSetName = "main";

  testScript = pkgs.writeShellScript "${config.name}-test-script.sh" ''
    set -xeu
    export PATH=${lib.makeBinPath cfg.packages}:$PATH
    ${cfg.script}
  '';

  waitOnlineScript = pkgs.writeShellScript "${config.name}-wait-online-inner-script.sh" ''
    set -xeu
    export PATH=${lib.makeBinPath cfg.packages}:$PATH
    ${cfg.waitOnline.script}
  '';

  waitOnlineScriptFinal = pkgs.writeShellScript "${config.name}-wait-online-script.sh" ''
    for i in {1..${toString cfg.waitOnline.retries}}; do
      # run script and ensure exit code 0
      timeout ${toString cfg.waitOnline.timeout}s ${waitOnlineScript}
      if [ $? -eq 0 ]; then
        exit 0
      fi

      # if exit code != 0, then sleep and retry
      sleep ${toString cfg.waitOnline.sleepBetween}
    done

    exit 1
  '';


  makeTest = let
    # OCTP 
    octp = import ./../octp.nix { pkgs = pkgs; };
    octpClient = "${octp}/bin/client";

    # OCTP Config
    octpConfigFile = pkgs.writeTextDir "octp/client.yml" (lib.generators.toYAML {} {
      challenge_sets.${challengeSetName} = config.build.basicresult;
    });
  in (import <nixpkgs/nixos/tests/make-test-python.nix> {
    # TODO(eyJhb): Debug, remove this later
    skipLint = true;
    # interactive = false;
  
    name = "${config.name}-test";
    nodes.hades = { pkgs, ...}: {
      virtualisation.diskSize = 5 * 1024;
      virtualisation.docker = {
        enable = true;
      };

      networking.firewall.enable = false;

      imports = [
        <nixpkgs/nixos/tests/common/x11.nix>
      ];
  
      environment.systemPackages = with pkgs; [
        firefox
        curl
        wget
        iputils
        
        # python
        (python38.withPackages (ps:
          with ps; [
            pyyaml
        ]))
      ];
    };
  
    testScript = let
      script = pkgs.writeShellScript "testscript" ''
        set -xeu
        # setup XDG_CONFIG_HOME for our octp config file
        export XDG_CONFIG_HOME=${octpConfigFile}

        # get local_ip etc.
        python ${./testing-help.py} > testing.env
        source testing.env

        # make and run the challenge
        ${octpClient} challenge make ${challengeConfig.id}
        echo ${octpClient} challenge start --public-address=$LOCAL_IPv4 ${challengeConfig.id}
        ${octpClient} challenge start --public-address=$LOCAL_IPv4 ${challengeConfig.id}

        # expose the basic information ie. VIRTUAL_<NAME>_IP, VIRTUAL_<NAME>_PORT_<NAME>
        python ${./testing-help.py} ${config.build.basicresult}/challenge.yml > testing.env
        source testing.env

        # run waitonline script here
        ${waitOnlineScriptFinal}
        
        # run the actual test
        timeout ${toString cfg.timeout} ${testScript}
      '';
    in ''
      start_all()
      hades.wait_for_unit("docker.service")

      print(hades.succeed("${script}"))
    '';
  });

  waitOnlineOpts = {
    options = {
      retries = lib.mkOption {
        type = types.int;
        default = 10;
      };
      sleepBetween = lib.mkOption {
        type = types.int;
        default = 1;
        description = "time to sleep between each";
      };
      timeout = lib.mkOption {
        type = types.int;
        default = 5;
        description = "timeout in seconds when executing script";
      };
      script = lib.mkOption {
        type = types.oneOf [ types.lines types.package ];
        default = "";
      };
    };
  };
in {
  options.testing = {
    test = lib.mkOption {
      # type = types.package;
      default = if cfg.script == "" && cfg.waitOnline.script == "" then (_: "") else makeTest;
    };

    packages = lib.mkOption {
      type = types.listOf types.package;
      default = with pkgs; [
        # basic core things
        coreutils

        python3

        # network stuff
        curl
        netcat-openbsd
      ];
    };

    script = lib.mkOption {
      type = types.oneOf [ types.lines types.package];
      default = "";
    };

    timeout = lib.mkOption {
      type = types.int;
      default = 120;
      description = "timeout in seconds when executing test script";
    };

    waitOnline = lib.mkOption {
      type = types.submodule waitOnlineOpts;
      default = {};
    };
  };
}

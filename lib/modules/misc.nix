{ config, pkgs, lib, ... }:

let
  cfg = config;

  inherit (lib) types;



  # TODO(eyJhb): maybe change this?
  # doesn't seem like the best way to
  # split up the logic....
  # also, the lib should be simplified.
  combined = /state/home/projects/deviosec/challenge-repos/combined;
  nlib = pkgs.callPackage "${combined}/lib" {};
in {
  options = {
    # TODO(eyJhb): maybe rename this to ID?
    # maybe this should just be fetched from the config
    name = lib.mkOption {
      type = types.str;
      description = "id of the challenge";
      default = cfg.challenge.config.id;
    };

    src = lib.mkOption {
      type = types.nullOr types.path;
      default = null;
    };

    # resulting stuff
    images = lib.mkOption {
      type = types.attrsOf types.package;
      readOnly = true;
      default = lib.mapAttrs' (_: v: lib.nameValuePair v.name v.config.build.image) config.nodes;
    };
  };
}

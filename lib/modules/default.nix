{
  imports = [
    ./overrides.nix

    ./nodes.nix
    ./misc.nix
    ./testing.nix
    ./build.nix
    ./challenge.nix
  ];
}

{ config, lib, ... }:

let
  inherit (lib) types;

in {
  options.overrides = {
    challenge = lib.mkOption {
      # type = types.functionTo types.attr;
      description = "override challenge configs";
      default = conf: conf; 
    };
  };
}

#!/usr/bin/env nix-shell
#!nix-shell --pure -i python3 -p "python3.withPackages (ps: with ps; [ pyyaml ])"
import subprocess
import yaml
import json
import sys

labPrefix = "testlab-"

def getContainers():
    output = subprocess.check_output("docker ps --format '{{.Names}} {{.ID}}'", shell=True)
    cnts = {}
    for cnt in output.decode("ascii").split("\n"):
        if cnt == "":
            continue
        s = cnt.split(" ")
        cnts[s[0]] = s[1]
    return cnts

def getContainerPorts(id):
    output = subprocess.check_output("docker inspect "+id, shell=True)

    data = json.loads(output)
    net = data[0]["NetworkSettings"]["Ports"]
    return net

def parseChallengeFile(filename):
    f = open(filename, "r")
    data = yaml.safe_load(f)
    return data

def getChallengePorts(config):
    # [<virtname>][<port>/<protocol>] = <portname>
    ports = {}
    for (_, virt) in enumerate(config["lab"]["virtuals"]):
        ports[virt["name"]] = {}
        for portname, port in virt["ports"].items():
            ports[virt["name"]][str(port["guest"])+"/"+port["protocol"]] = portname 

    return ports

def getLocalIPs():
    outputipv6 = subprocess.check_output("ip -6 addr | grep inet6 | awk -F '[ \t]+|/' '{print $3}' | grep -v ^::1 | grep -v ^fe80 | head -n1", shell=True)
    outputipv4 = subprocess.check_output("ip -4 addr | grep inet | awk -F '[ \t]+|/' '{print $3}' | grep -v '^127' | grep '10.' | head -n1", shell=True)
    return [ outputipv6.strip().decode("utf-8"), outputipv4.strip().decode("utf-8") ]

# list of envs we want
envs = []

# get local IPs
localIPv6, localIPv4 = getLocalIPs()
envs.append("export LOCAL_IP={}".format(localIPv6))
envs.append("export LOCAL_IPv6={}".format(localIPv6))
envs.append("export LOCAL_IPv4={}".format(localIPv4))

# if we have a challenge file, use that as well
if len(sys.argv) > 1:
    cnts = getContainers()
    config = parseChallengeFile(sys.argv[1])
    cports = getChallengePorts(config)

    # fixup container ports
    fports = {}
    for (name, id) in cnts.items():
        rname = name[len(labPrefix+config["id"]+"-"):]
        fports[rname] = {}
        cntPorts = getContainerPorts(id)
        for (portName, port) in cntPorts.items():
            configPortName = cports[rname][portName]
            fports[rname][configPortName] = int(port[0]["HostPort"])

    for (nodeName, ports) in fports.items():
        upperNodeName = nodeName.upper()

        envs.append("export VIRTUAL_{}_IP={}".format(upperNodeName, "::1"))
        envs.append("export VIRTUAL_{}_IPv6={}".format(upperNodeName, "::1"))
        envs.append("export VIRTUAL_{}_IPv4={}".format(upperNodeName, "127.0.0.1"))
        for (portName, port) in ports.items():
            envs.append("export VIRTUAL_{}_PORT_{}={}".format(upperNodeName, portName.upper(), port))

print("\n".join(envs))

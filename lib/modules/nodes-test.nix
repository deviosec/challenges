{ config, pkgs, lib, ... }:

let
  inherit (lib) types;
in {
  options = {
    name = lib.mkOption {
      type = types.str;
    };
    test = lib.mkOption {
      type = types.package;
      default = pkgs.hello;
    };

  };
}

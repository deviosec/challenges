{ config, pkgs, lib, ... }:

let
  cfg = config.services.mysql;

  inherit (lib) types;

  # helpers
  settingsFile = pkgs.writeText "my.cnf" (
    lib.generators.toINI { listsAsDuplicateKeys = true; } cfg.settings
  );
  settingsFileWRoot = pkgs.writeText "my.cnf" (
    lib.generators.toINI { listsAsDuplicateKeys = true; }
  );

  # opts
  accessOpts = {
    options = {
      database = lib.mkOption {
        type = types.str;
        # default = "*";
      };
      table = lib.mkOption {
        type = types.str;
        default = "*";
      };
      host = lib.mkOption {
        type = types.str;
        default = "localhost";
      };
      privileges = lib.mkOption {
        type = types.listOf types.str;
        default = [
          "INSERT"
          "SELECT"
          "UPDATE"
          "DELETE"
        ];
      };
    };
  };
  userOpts = { name, ... }: {
    options = {
      username = lib.mkOption {
        type = types.str;
        default = name;
      };
      password = lib.mkOption {
        type = types.nullOr types.str;
        default = null;
      };
      host = lib.mkOption {
        type = types.str;
        default = "localhost";
      };
      access = lib.mkOption {
        type = types.attrsOf (types.submodule accessOpts);
      };
    };
  };

  # .databases.<database_name> = { name = "something"; schema = <path-to-file>; sql_files = DAG_SORTED { name = <path-to-file>; };
  databaseOpts = { name, ... }: {
    options = {
      name = lib.mkOption {
        type = types.str;
        default = name;
      };
      # schema = lib.mkOption {
      #   type = types.nullOr types.path;
      #   default = null;
      #   description = "this schema is run at build time";
      # };
      sqlFiles = lib.mkOption {
        # type = types.dagOf types.path;
        type = types.listOf types.path;
        default = [];
        description = "these SQL files are run at build time";
      };
    };
  };
in {
  options.services.mysql = {
    enable = lib.mkEnableOption "enable mysql server";

    package = lib.mkOption {
      type = types.package;
      default = pkgs.mysql;
    };

    initializeType= lib.mkOption {
      type = types.enum [ "mysql" "mariadb" ];
      default = "mariadb";
    };

    bind = lib.mkOption {
      type = types.str;
      default = "localhost";
    };

    port = lib.mkOption {
      type = types.int;
      default = 3306;
    };

    settings = lib.mkOption {
      type = types.attrsOf types.anything;
      default = {};
    };

    users = lib.mkOption {
      type = types.attrsOf (types.submodule userOpts);
      default = {};
    };

    databases = lib.mkOption {
      type = types.attrsOf (types.submodule databaseOpts);
      default = {};
    };
  };

  config = lib.mkIf cfg.enable {
    # set default bind + port
    services.mysql.settings.mysqld = {
      bind_address = lib.mkDefault cfg.bind;
      port = lib.mkDefault cfg.port;
      user = config.users.users.mysql.name;

      # datadir, socket and pid file
      datadir = "/var/lib/mysql";
      "pid-file" = "/run/mysqld/mysqld.pid";
      socket = "/run/mysqld/mysqld.sock";
    };

    # setup user
    users.users.mysql = {};
    users.groups.mysql.members = [ "mysql" ];

    # place configuration file
    files.files."/etc/my.cnf".file = settingsFile;

    build.buildCommands.mysql-setup = lib.dag.entryAfter [ "setup" ] ''
      # setup data directory
      mkdir -p /var/lib/mysql
      chown mysql:mysql /var/lib/mysql

      # setup socket dir
      mkdir -p /run/mysqld
      chown mysql:mysql /run/mysqld
    '';

    build.buildCommands.mysql-init-user-databases = let
      mysqlImportCMD = file: db: "cat ${file} | ${cfg.package}/bin/mysql -uroot" + (if db != null then " -D ${db}" else "");
      # mysqlImportCMD = file: db: "cat ${file} | ${cfg.package}/bin/mysql -uroot" + (if db != null then " -D ${db}" else "");

      initialSQL = let
        users = lib.flatten (lib.mapAttrsToList (_: user:
          [ ("CREATE USER IF NOT EXISTS '${user.username}'@'${user.host}'" + (if user.password != null then " IDENTIFIED BY '${user.password}'" else "") + ";") ]
          ++ (lib.mapAttrsToList(_: access:
            "GRANT ${lib.concatStringsSep '','' access.privileges} ON ${access.database}.${access.table} TO '${user.username}'@'${access.host}';"
          ) user.access)
        ) cfg.users);

        databases = lib.mapAttrsToList (_: db: "CREATE DATABASE IF NOT EXISTS ${db.name};") cfg.databases;
      in pkgs.writeText "initial.sql" (lib.concatStringsSep "\n" (users ++ databases));

      importSQLs = lib.flatten (lib.mapAttrsToList (_: db: lib.forEach db.sqlFiles (sqlFile: mysqlImportCMD sqlFile db.name)) cfg.databases);
    in lib.dag.entryAfter [ "mysql-setup" ] ''
      export PATH=${lib.makeBinPath (with pkgs; [ gnused ])}:$PATH

      ${if cfg.initializeType == "mysql"
        then "${cfg.package}/bin/mysqld --defaults-file=/etc/my.cnf --initialize-insecure --user=mysql"
        else "${cfg.package}/bin/mysql_install_db --defaults-file=/etc/my.cnf --datadir=${cfg.settings.mysqld.datadir} --user=mysql"
      }
      ${cfg.package}/bin/mysqld &

      # wait for server to come online
      while ! ${cfg.package}/bin/mysql -uroot >> /dev/null; do
        sleep 1
      done

      # import all sql files
      ${lib.concatStringsSep "\n" ([ (mysqlImportCMD initialSQL null) ] ++ importSQLs)}

      # kill server
      ${pkgs.procps}/bin/pkill -F /run/mysqld/mysqld.pid
    '';

    # start mysqld
    services.start.mysql = let
    in lib.dag.entryAnywhere {
      command = "${cfg.package}/bin/mysqld";
      user = config.users.users.mysql.name;
    };
  };
}

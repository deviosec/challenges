{ config, pkgs, lib, ... }:

# TODO(eyJhb): make this part of the nginx module
# as this heavily depend on nginx, and if we just want
# PHP, we can just use it without nginx.
let
  cfg = config.services.php;

  inherit (lib) types;

  # helper commands
  mkPool = pool: ''
    [${pool.name}]
    ${lib.concatStringsSep "\n" (lib.mapAttrsToList (n: v: "${n} = ${toString v}") pool.settings)}
  '';

  # https://github.com/NixOS/nixpkgs/blob/74d017edb6717ad76d38edc02ad3210d4ad66b96/nixos/modules/services/web-servers/phpfpm/default.nix#L101
  subModulePool = { name, config, ... }: {
    options = {
      name = lib.mkOption {
        type = types.str;
        default = name;
      };

      settings = lib.mkOption {
        type = with types; attrsOf (oneOf [ str int bool ]);
        default = {};
      };
    };
  };
    
in {
  options.services.php = {
    enable = lib.mkEnableOption "enable php nginx host";

    package = lib.mkOption {
      type = types.package;
      default = pkgs.php;
    };

    pools = lib.mkOption {
      type = types.attrsOf (types.submodule subModulePool);
      default = {};
    };

  };

  config = lib.mkIf cfg.enable {
    # TODO(eyJhb): maybe this should not be here?
    services.php.pools.global.settings = lib.mapAttrsRecursive (path: v: lib.mkDefault v) {
      # do not daemonize
      "daemonize" = "no";

      error_log = "/var/log/php-fpm.log";
    };

    services.php.pools.www.settings = lib.mapAttrsRecursive (path: v: lib.mkDefault v) {
      # user
      "user" = "php";
      "group" = "nginx";

      # listen stuff
      "listen" = "/run/php/php8.0-fpm.sock";
      "listen.owner" = "php";
      "listen.group" = "nginx";
      "listen.mode" = "0660";

      "pm" = "dynamic";
      "pm.max_children" = 5;
      "pm.start_servers" = 2;
      "pm.min_spare_servers" = 1;
      "pm.max_spare_servers" = 3;

      # "php_flag[display_errors]" = "on";
      "php_flag[display_errors]" = "off";
    };

    # create php user and add it to nginx group
    users.users.php = {};
    users.groups.nginx.members = [ "php" ];

    # TODO(eyJhb): this should be based on the configuration we have
    # and not just hardcoded
    build.activation.php = lib.dag.entryAfter [ "setup" ] ''
      mkdir -p /run/php
      chown nginx:php /run/php
    '';

    services.start.php = let
      phpFPMConfig = pkgs.writeText "php-fpm" (lib.concatStringsSep "\n\n" (lib.mapAttrsToList (n: v: mkPool v) cfg.pools));
    in lib.dag.entryBefore [ "nginx" ] {
      command = "${cfg.package}/bin/php-fpm --fpm-config ${phpFPMConfig}";
      user = "root";
    };
  };
}

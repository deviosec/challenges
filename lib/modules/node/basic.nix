{ node, config, pkgs, lib, ... }:

let
  inherit (lib) types;

  portOpts = {
    option = {
      port = lib.mkOption {
        type = types.int;
      };
      protocol = lib.mkOption {
        type = types.enum [ "tcp" "udp" ];
        default = "tcp";
      };
    };
  };
in {
  options = {
    hostname = lib.mkOption {
      type = types.str;
      default = node.name;
    };
    cwd = lib.mkOption {
      type = types.str;
      default = "";
    };
    ports = lib.mkOption {
      type = types.attrsOf (types.submodule portOpts);
      default = {};
    };
  };

  # TODO(eyJhb): implement
  config = {
    files.files."/etc/hosts" = {
      file = let
        hostnames = [ "localhost" config.hostname ];
        ips = [ "127.0.0.1" "::1" ];
        content = lib.concatStringsSep "\n" (lib.flatten (
          lib.forEach hostnames (
          hostname: (lib.forEach
              ips (ip: "${ip} ${hostname}")
            )
          )
        ));
      in pkgs.writeText "hosts" content;
      permissions  = "u=rw,og=r";
    };
  };
}

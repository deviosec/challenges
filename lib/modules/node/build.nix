{ topconfig, config, node, pkgs, lib, ... }:

let
  cfg = config.build;

  inherit (lib) types;
in {
  options.build = {
    phases = lib.mkOption {
      type = types.dagOf types.package;
      default = {};
    };

    activation = lib.mkOption {
      type = types.dagOf (types.oneOf [ types.str types.package ]);
      default = {};
    };

    activationScript = lib.mkOption {
      type = types.package;
    };

    postbuild = lib.mkOption {
      type = types.dagOf types.lines;
    };

    buildCommands = lib.mkOption {
      type = types.dagOf types.lines;
      default = {};
    };

    image = lib.mkOption {
      type = types.package;
    };

    packages = lib.mkOption {
      type = types.listOf types.package;
      default = [];
    };
  };

  config = {
    # setup different phases of activation
    # build.activation = {
    build.buildCommands = {
      setup = lib.dag.entryBefore [ "run" ] ''
        mkdir /tmp
        chmod 777 /tmp

        mkdir -p /var/log
        chmod 777 /var/log
      '';
      run = lib.dag.entryAfter [ "preSetup" ] "";
    };

    # build activationScript
    build.activationScript = let
      sortedActivation = lib.forEach ((lib.dag.topoSort cfg.activation).result) (x: x.data);
    in pkgs.writeShellScript "init.sh" ((builtins.concatStringsSep "\n" sortedActivation) + "\n");

    build.image = let
      root = pkgs.symlinkJoin {
        name = "octp-image";
        paths = lib.forEach ((lib.dag.topoSort cfg.phases).result) (x: x.data);
      };

      # TODO(eyJhb) fix this cp stuff
      rootImage = pkgs.runCommand "postbuild" { buildInputs = [ root ]; } ''
        set -xeu
        # initial stuff
        mkdir -p $out
        cp -a ${root}/. $out
        chmod 700 -R $out

        # TODO(eyJhb): post build stuff here

        # add our activation script here maybe? Dunno
        # isn't really needed, so unsure if this will just
        # open up for bad things
        cp ${cfg.activationScript} $out/init.sh
      '';

      # phase 1
      # -------
      # build a layered image, so that we can
      # use it in the next stage, where we manage
      # all the permissions etc.
      # the reason we still use the layered image
      # is because we want the ability to reuse
      # layers between containers.
      image1p1 = pkgs.dockerTools.buildLayeredImage {
        name = "image1-stage1";
        contents = [ pkgs.coreutils ] ++ config.build.packages;

        config = {
          # set entrypoint, so that we have layers here as well
          EntryPoint = [ config.build.activationScript ];
        };
      };

      image1p2 = pkgs.dockerTools.buildImage {
        name = "${topconfig.name}-${node.name}";
        tag = node.tag;

        diskSize = 4 * 1024;

        # inherit phase 1 image
        fromImage = image1p1;

        contents = [
          rootImage
        ];

        runAsRoot = let
          cmds = lib.forEach (lib.dag.topoSort cfg.buildCommands).result (x: "## ${x.name}\n${x.data}");
        in ''
          set -xeu
          # build commands
          ${lib.concatStringsSep "\n" cmds}
        '';

        # set the correct configuration here
        # keep in mind, nothing is inherited from
        # the previous image
        config = {
          EntryPoint = [ config.build.activationScript ];
          Hostname = config.hostname;
          WorkingDir = config.cwd;
          ExposedPorts = lib.mapAttrs' (_: v: lib.nameValuePair "${toString v.port}/${v.protocol}") config.ports;
        };
      };
    in image1p2;
  };
}

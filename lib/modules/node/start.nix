{ config, nodename, lib, pkgs, ... }:


let
  cfg = config.services.start;
  cfgUsers  = config.users;

  inherit (lib) types; 

  # TODO(eyJhb): there needs to be some kind of priority
  startOpts = { name, ... }: {
    options = {
      name = lib.mkOption {
        type = types.str;
        default = name;
      };
      command = lib.mkOption {
        type = types.oneOf [ types.str types.package ];
      };
      user = lib.mkOption {
        type = types.str;
        default = "nobody";
      };
      environment = lib.mkOption {
        type = types.attrsOf types.str;
        default = {};
      };
      autorestart = lib.mkOption {
        type = types.bool;
        default = true;
      };
      disableASLR = lib.mkOption {
        type = types.bool;
        default = false;
      };
    };
  };
in {
  options.services.start = lib.mkOption {
    type = types.dagOf (types.submodule startOpts);
    default = {};
  };

  config.build.activation.start = let
    sortedStarts = lib.forEach (lib.dag.topoSort cfg).result (v: v.data // { name = v.name; });
    programs = lib.listToAttrs (lib.imap0 (i: v: lib.nameValuePair "program:${v.name}" {
      command = if v.disableASLR
                then "${pkgs.util-linux}/bin/setarch `${pkgs.coreutils}/bin/uname -a` -R ${v.command}"
                else v.command;
      user = v.user;
      autostart = true;
      autorestart = v.autorestart;
      environment = let
        defaultEnvOpts = { USER = v.user; HOME = cfgUsers.users."${v.user}".home; };
        envList = lib.mapAttrsToList (n: v: "${n}=${v}") (defaultEnvOpts // v.environment);
      in lib.concatStringsSep "," envList;


      priority = i;
    }) sortedStarts);
    config = {
      supervisord = {
        nodaemon = true;
        silent = true;

        # loglevel = "critical";
        # logfile = "/tmp/supervisord.log";
        logfile_maxbytes = 0;
      };
    } // programs;
    configFile = pkgs.writeText "supervisord.conf" (lib.generators.toINI {} config);
  in lib.dag.entryAfter [ "setup" ] "${pkgs.python3Packages.supervisor}/bin/supervisord -c ${configFile}";
}

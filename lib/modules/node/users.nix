{ config, lib, pkgs, ... }:

## TODO(eyJhb) support /etc/shadow as well

let
  inherit (lib) types;

  cfg = config.users;

  subModuleUser = { config, name, ... }: {
    options = {
      name = lib.mkOption {
        type = types.str;
        default = name;
      };
      password = lib.mkOption {
        type = types.nullOr types.str;
        default = null;
      };
      uid = lib.mkOption {
        type = types.nullOr types.int;
        default = null;
      };
      gid = lib.mkOption {
        type = types.nullOr types.int;
        default = null;
      };
      comment = lib.mkOption {
        type = types.str;
        default = "";
      };
      home = lib.mkOption {
        type = types.str;
        default = "/var/empty";
      };
      shell = lib.mkOption {
        type = types.str;
        default = "/sbin/nologin";
      };
    };
  };

  subModuleGroup = { config, name, ... }: {
    options = {
      name = lib.mkOption {
        type = types.str;
        default = name;
      };
      gid = lib.mkOption {
        type = types.nullOr types.int;
        default = null;
      };
      members = lib.mkOption {
        type = types.nullOr (types.listOf types.str);
        default = null;
      };
    };
  };

  mkPassword = passwd: builtins.readFile (pkgs.runCommand "password" { buildInputs = [ pkgs.mkpasswd ]; } ''
    echo ${passwd} | mkpasswd -m sha-512 --stdin | tr -d '\n' > $out
  '');
  
  # TODO(eyJhb): Fix this function - password hash gets chomped
  mkUser = user: let 
    password = if (user.password != null) then (mkPassword user.password) else "x";
  in ''${user.name}:${password}:${toString user.uid}:${toString user.gid}:${user.comment}:${user.home}:${user.shell}'';

  mkGroup = group: let
    members = if (group.members != null) then builtins.concatStringsSep "," group.members else "";
  in ''${group.name}::${toString group.gid}:${members}'';

  # usersAssignIDs will assign UID and GID for users
  # which does not currently have any.
  usersAssignIDs = uidStart: gidStart: users: let
    # convert attrs to list
    usersList = lib.attrValues users;

    # check if id is already occupied
    idUsed = key: users: id: lib.length (lib.filter (v: v.${key} == id) users) > 0;

    assignUser = key: i: users: id: let
      user = lib.elemAt users i;
    in if lib.length users == i
      then []
       else if user.${key} != null
        then [ user ] ++ (assignUser key (i+1) users id)
        else if idUsed key users id
          then assignUser key i users (id+1)
        else [ (user // { ${key} = id; }) ] ++ (assignUser key (i+1) users (id+1));

    fixupUIDs = assignUser "uid" 0 usersList uidStart;
    fixupGIDs = assignUser "gid" 0 fixupUIDs gidStart;
  in lib.listToAttrs (builtins.map (v: { name = v.name; value = v; }) fixupGIDs);

  # createMissingGroups will return a attrset of
  # users with a GID but no group matching that GID.
  # Returns the resulting groups
  createMissingGroups = users: groups: let
    groupsList = lib.attrValues groups;
    usersList = lib.attrValues users;

    groupExists = groupid: lib.length (lib.filter (v: v.gid == groupid) groupsList) > 0;
    usersWithoutGroup = lib.filter (v: if groupExists v.gid then false else true) usersList;
    newGroups = builtins.map (v: { name = v.name; value = { name = v.name; gid = v.gid; members = [ v.name ]; }; }) usersWithoutGroup;
  in lib.recursiveUpdate groups (lib.listToAttrs newGroups);
in {
  options = {
    users = {
      uidStart = lib.mkOption {
        type = types.int;
        default = 500;
      };
      gidStart = lib.mkOption {
        type = types.int;
        default = 500;
      };
      users = lib.mkOption {
        type = types.nullOr (types.attrsOf (types.nullOr (types.submodule subModuleUser)));
      };
      groups = lib.mkOption {
        type = types.nullOr (types.attrsOf (types.nullOr (types.submodule subModuleGroup)));
      };
    };
  };
  config = let
    users = usersAssignIDs cfg.uidStart cfg.gidStart cfg.users;
    groups = createMissingGroups users cfg.groups;
  in {
    # create root and nobody user
    users = {
      users.root = {
        uid = 0;
        gid = 0;
      };
      users.nobody = {
        uid = 65534;
        gid = 65534;
      };
      groups.root = {
        gid = 0;
        members = [ "root" ];
      };
      groups.nobody = {
        gid = 65534;
        members = [ "nobody" ];
      };
    };

    # setup buildphase
    build.phases.setupUsers = lib.dag.entryAnywhere (pkgs.symlinkJoin {
      name = "users";
      paths = let
        usersLines = builtins.concatStringsSep "\n" (lib.mapAttrsToList (n: v: mkUser v) (lib.filterAttrs (n: v: v != null) users));
        groupsLines = builtins.concatStringsSep "\n" (lib.mapAttrsToList (n: v: mkGroup v) (lib.filterAttrs (n: v: v != null) groups));

        # apply = x: assignIDs uidStart gidStart x;
      in [
        (pkgs.writeTextDir "etc/passwd" (usersLines + "\n"))
        (pkgs.writeTextDir "etc/group" (groupsLines + "\n"))
        (pkgs.writeTextDir "etc/nsswitch.conf" ''
              hosts: files dns
          '')
        (pkgs.runCommand "var-empty" { } ''
              mkdir -p $out/var/empty
          '')
      ];
    });

    build.buildCommands.setupUserHomes = let
      # filter users with `/var/empty`
      filteredUsers = lib.filterAttrs (_: v: v.home != "/var/empty") users;
      createHomeCMDsList = lib.mapAttrsToList (n: v:
        "mkdir -p ${v.home} && chown ${toString v.uid}:${toString v.gid} ${v.home} && chmod 700 ${v.home}"
      ) filteredUsers;
      createHomeCMDs = lib.concatStringsSep "\n" createHomeCMDsList; 
    in lib.dag.entryAfter [ "run" ] createHomeCMDs;
  };
}

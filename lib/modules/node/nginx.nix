{ config, pkgs, lib, ... }:

# TODO(eyJhb): we need to add a settingsfile, so that we can just refer to that.
# this is nice if a user wants to copy the settings file to a generic location
# instead of in the /nix/store, maybe ability to change what settingsFile we run?
# unsure how this is done best

let
  cfg = config.services.nginx;

  inherit (lib) types;

  # TODO(eyJhb) maybe we can run a nginx config formatter
  # https://github.com/slomkowski/nginx-config-formatter
  mkSettingsToList = set: lib.mapAttrsToList (n: v:
      if lib.isList v
      then lib.concatStringsSep "\n" (lib.forEach v (x: "${n} ${x};"))
      else if lib.isBool v
        then "${n} ${if v then "on" else "off"};"
        else "${n} ${toString v};"
    ) set;
  mkSettings = settings: lib.concatStringsSep "\n" (mkSettingsToList settings);

  # helpers
  mkVHost = vhost: let
    settings = mkSettings vhost.settings;

    locations = lib.mapAttrsToList (n: v: ''
      location ${v.location} {
        ${mkSettings v.settings}
      }
    '') vhost.locations;
    locationsLines = lib.concatStringsSep "\n" locations;
  in ''
    server {
      ${settings}
      ${locationsLines}
    }
  '';

  mkConfig = conf: let
    vhosts = lib.mapAttrsToList (n: v: mkVHost v) conf.virtualHosts;
    vhostsLines = lib.concatStringsSep "\n" vhosts;
  in ''
    ${mkSettings conf.settings}

    events {
      ${mkSettings commonEventsConfig}
    }

    http {
      ${mkSettings commonHTTPConfig}
  
      ${vhostsLines}
    }
  '';

  # default config
  commonHTTPConfig = {
    # logs
    access_log = "/var/log/nginx/access.log";
    error_log = "/var/log/nginx/error.log";

    # mime types and default params
    include = [
      "${cfg.package}/conf/mime.types"
      "${cfg.package}/conf/fastcgi.conf"
    ];
    default_type = "application/octet-stream";

    # tcp stuff
    sendfile = true;
    tcp_nopush = true;
    tcp_nodelay = true;
    keepalive_timeout = 65;
    types_hash_max_size = 4096;

    # compression
    gzip = true;
  };

  commonEventsConfig = {
    worker_connections = 2048;
  };

  subModuleLocation = { name, config, ... }: {
    options = {
      location = lib.mkOption {
        type = types.str;
        default = name;
      };
      settings = lib.mkOption {
        type = with types; attrsOf (oneOf [ str int bool (listOf str) ]);
        default = {};
      };
    };
  };

  subModuleVHost = { name, config, ... }: {
    options = {
      name = lib.mkOption {
        type = types.str;
        default = name;
      };

      settings = lib.mkOption {
        type = with types; attrsOf (oneOf [ str int bool (listOf str) ]);
        default = {};
      };

      locations = lib.mkOption {
        type = types.attrsOf (types.submodule subModuleLocation);
        default = {};
      };
    };
  };
in {
  options.services.nginx = {
    enable = lib.mkEnableOption "enable nginx server";

    package = lib.mkOption {
      type = types.package;
      default = pkgs.nginx;
    };

    settings = lib.mkOption {
      type = with types; attrsOf (oneOf [ str int bool ]);
    };

    virtualHosts = lib.mkOption {
      type = types.attrsOf (types.submodule subModuleVHost);
      default = {};
    };
  };

  config = lib.mkIf cfg.enable {
    services.nginx = lib.mapAttrsRecursive (path: v: lib.mkDefault v) {
      settings = {
        # do not run in background
        daemon = "off";

        # run on nginx user and group
        user = "nginx nginx";

        # error_log = "/var/log/nginx/error.log";
      };

      virtualHosts.default = {
        # name = "default_server";
        settings = {
          listen = [
            "80 default_server"
            "[::]:80 default_server"
          ];
          root = "/var/www/html";
          index = "index.php index.html index.htm";
        };

        # TODO(eyJhb): maybe this should be placed in PHP instead, or rather
        # it should obey the config.services.php.enable
        locations = {
          # root handler
          # this gets the index and root from the parent settings
          "/" = {
            settings = {};
          };

          # just have a seperate location to match
          # all our static content
          static-files = {
            location = "~* \.(css|js|gif|jpe?g|png)$";
          };

          php-files = {
            location = "~* \\.php$";
            settings = {
                # setup the fastcgi_pass, based on the www settings listen
                # TODO(eyJhb): maybe not just assume www pool?
                fastcgi_pass = let
                  listenOption = config.services.php.pools.www.settings.listen;
                  phpfpmListen = if lib.hasPrefix "/" listenOption
                    then "unix:${listenOption}"
                    else listenOption;
                in phpfpmListen;

                fastcgi_index = "index.php";

                include = "${cfg.package}/conf/fastcgi_params";
                fastcgi_param = [
                  "SCRIPT_FILENAME $document_root$fastcgi_script_name"
                  "SCRIPT_NAME $fastcgi_script_name"
                ];
            };
          };
        };
      };
    };

    users = {
      users.nginx = {};
      groups.nginx.members = [ "nginx" ];
    };

    # TODO(eyJhb): this should be based on the configuration we have
    # and not just hardcoded
    build.activation.nginx = lib.dag.entryAfter [ "setup" ] ''
      # services.nginx
      mkdir -p /var/log/nginx
      mkdir -p /var/cache/nginx
      mkdir -p /var/www/html/
      chown nginx:nginx /var/log/nginx /var/cache/nginx /var/www/html
    '';

    services.start.nginx = let
      configFile = pkgs.writeText "nginx.conf" (mkConfig cfg);
    in lib.dag.entryAfter [ "php" ] {
      command = "${cfg.package}/bin/nginx -c ${configFile}";
      user = "nginx";
    };
  };
}

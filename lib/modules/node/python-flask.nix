{ config, pkgs, lib, ... }:

let
  cfg = config.services.python-flask;

  inherit (lib) types;
in {
  options.services.python-flask = {
    enable = lib.mkOption {
     type = types.bool;
      default = false;
    };

    package = lib.mkOption {
      type = types.package;
      default = pkgs.python3.withPackages(ps: with ps; [ flask ]);
    };

    user = lib.mkOption {
      type = types.str;
      default = "app";
    };

    # extra stuff
    production = lib.mkOption {
      type = types.bool;
      default = true;
    };

    bind = lib.mkOption {
      type = types.str;
      default = "0.0.0.0";
    };

    port = lib.mkOption {
      type = types.int;
      default = 80;
    };

    apppath = lib.mkOption {
      type = types.str;
      default = "/srv/app/app.py";
    };
  };

  config = lib.mkIf cfg.enable {
    services.start.app = lib.dag.entryAnywhere {
      command = pkgs.writeShellScript "app" ''
        export FLASK_ENV=${if cfg.production then "production" else "development"}
        export FLASK_APP=${cfg.apppath}
        export FLASK_HOST=${cfg.bind}
        export FLASK_PORT=${toString cfg.port}

        ${cfg.package}/bin/python -m flask run --host $FLASK_HOST --port $FLASK_PORT
      '';

      user = cfg.user;
    };

    users.users.${cfg.user} = {};
  };
}

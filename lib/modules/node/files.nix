{ pkgs, config, lib, ... }:

let
  cfg = config.files;

  inherit (lib) types;

  defaultOpts = name: {
    destination = lib.mkOption {
      type = types.str;
      default = name;
    };
    user = lib.mkOption {
      type = types.str;
      default = "root";
    };
    group = lib.mkOption {
      type = types.str;
      default = "root";
    };
    permissions = lib.mkOption {
      type = types.oneOf [ types.int types.str ];
      default = "ug=rX,o=";
    };
  };

  filesOpt = { name, ... }: {
    options = {
      file = lib.mkOption {
        type = types.path;
      };
    } // (defaultOpts name);
  };

  dirsOpt = { name, ... }: {
    options = {
      dir = lib.mkOption {
        type = types.path;
      };
      recurseOwner = lib.mkOption {
        type = types.bool;
        default = true;
      };
      recursePermissions = lib.mkOption {
        type = types.bool;
        default = true;
      };
    } // (defaultOpts name);
  };
in {
  options.files = {
    files = lib.mkOption {
      type = types.attrsOf (types.submodule filesOpt);
      default = {};
    };
    dirs = lib.mkOption {
      type = types.attrsOf (types.submodule dirsOpt);
      default = {};
    };
  };

  config.build.buildCommands.files = let
    filesCommands = lib.forEach (lib.attrValues cfg.files) (v: ''
      mkdir -p $(dirname ${v.destination})
      cp ${v.file} ${v.destination}
      chown ${v.user}:${v.group} ${v.destination}
      chmod ${v.permissions} ${v.destination}
    '');

    dirsCommands = lib.forEach (lib.attrValues cfg.dirs) (v: ''
      mkdir -p ${v.destination}
      cp -a ${v.dir}/. ${v.destination}
      chown ${v.user}:${v.group} ${if v.recurseOwner then "-R" else ""} ${v.destination}
      chmod ${v.permissions} ${if v.recursePermissions then "-R" else ""} ${v.destination}
    '');
  in lib.dag.entryAfter [ "setup" ] ''
    # copying files
    ${lib.concatStringsSep "\n" filesCommands}
    # copying dirs
    ${lib.concatStringsSep "\n" dirsCommands}
  '';
}

{ lib }:

let
  inherit (lib) types;

  # opts
  flagsOpts = { name, ... }: {
    options = {
      name = lib.mkOption {
        type = types.str;
        default = name;
        description = ''
          What the name of the flag should be, typically this will be something like
          MAIN, if it is the only flag this challenges has. Keep in mind, this is the
          name that will be used in the virtual envs, as a placeholder, e.g. ##FLAG_MAIN##.
        '';
      };
      flag = lib.mkOption {
        type = types.str;
        description = "The actual flag, that users will find in the challenge";
        example = "AARHUSCTF2019{This_Is_A_Awesome_Calculator}";
      };
      dynamic_env = lib.mkOption {
        type = types.bool;
        default = false;
        description = "Indicated if the flag can be changed on starting the challenge, or that it is static.";
      };
      dynamic_build = lib.mkOption {
        type = types.bool;
        default = false;
        description = "Indicated if the flag can be changed if a rebuild is made of the challenge, or that it is static.";
      };
    };
  };

  hintsOpts = {
    options = {
      hint = lib.mkOption {
        type = types.str;
        description = "The hint that you want to give the contestant, that costs to get.";
        example = "Maybe it is running some kind of external command?";
      };
      cost = lib.mkOption {
        type = types.ints.between 0 100;
        description = "The cost of the hint, on a scale from 0 to 100. This is subject to future change!";
      };
    };
  };

  filesOpts = {
    options = {
      filename = lib.mkOption {
        type = types.str;
      };
      sha256 = lib.mkOption {
        type = types.str;
        default = "";
      };
      link = lib.mkOption {
        type = types.str;
        default = "";
      };
    };
  };

  portsOpts = {
    options = {
      port = lib.mkOption {
        type = types.int;
        description = "port number that the virtual exposes";
      };
      protocol = lib.mkOption {
        type = types.enum [ "tcp" "udp"];
        default = "tcp";
        description = "protocol that the port exposes";
      };
    };
  };

  virtualsOpts = { config, ... }: {
    options = {
      name = lib.mkOption {
        type = types.str;
        default = "main";
        description = "the name of the virtual";
      };
      hostname = lib.mkOption {
        type = types.str;
        default = config.name;
        description = "this defaults to the name, and is used by other virtuals to refer to this virtual in the network";
      };

      # TODO(eyJhb) we might have to add a option, to say it should be given to the user?

      ports = lib.mkOption {
        type = types.attrsOf (types.submodule portsOpts);
        default = { };
        # description = "Ports that the virtual should expose to the public. These ports will automatically get mapped to a port on the host";
      };
      envs = lib.mkOption {
        type = types.attrsOf types.str;
        default = {};
        example = ''{ "FLAG" = "##FLAG_MAIN##"; "PORT_HTTP" = "##PORT_HTTP##"; "THEME" = "au"; }'';
      };

      # up for change!!
      security_options = lib.mkOption {
        type = types.listOf types.str;
        default = [];
        example = ''
          [ "allow-aslr" ];
        '';
        description =
          "Security opt to set, which is labels for a MLS system, e.g. seccomp, etc.";
      };
    };
  };

  networksOpts = {
    options = {
      name = lib.mkOption {
        type = types.str;
        default = "default";
        description = "Name of the network, used only for indentification";
      };
      connections = lib.mkOption { type = types.listOf types.str; };
    };
  };

  module = {
    options = with lib; {
      id = lib.mkOption {
        type = types.str;
        example = "aarhusctf-2019-awesome-calculator";
      };
      title = lib.mkOption {
        type = types.str;
        example = "Awesome Calculator";
      };
      description = lib.mkOption {
        type = types.str;
        example = "Checkout this awesome web based calculator, it works great";
      };

      # TODO(eyJhb) we need a least a length of one!
      flags = lib.mkOption {
        type = types.attrsOf (types.submodule flagsOpts);
        example = ''
          {
            main = { flag = "FLAG{MainFlag!!}"; dynamic_env = true; };
          }
        '';
      };

      hints = lib.mkOption {
        type = types.listOf (types.submodule hintsOpts);
        example = ''
          [
            { hint = "Maybe it is runnig a external command?"; cost = 10; }
            { hint = "Didn't understand the first hint? Do some command injection!"; cost = 20; }
          ]
        '';
        default = [ ];
        description = ''
          Descripe the hints that a user might get, when they are stuck.
          You are able to specify multiple hints, which are the order that
          the hints will be available/shown.
        '';
      };

      category = lib.mkOption {
        type = types.enum [ "web" "reverse" "misc" "crypto" "bin" "pwn" "forensics" ];
        description = "The category that the challenge is under";
      };

      difficulty = lib.mkOption {
        type = types.ints.between 0 100;
        description = ''
          The difficulty of the challenge on a scale from 0 to 100.
          0 being very easy, 100 being almost impossible to do.
        '';
      };

      tags = lib.mkOption {
        type = types.listOf types.str;
        example = ''[ "aarhusctf" "aarhusctf2019" "2019" ]'';
        description = "tags for the challenge, which can be used for filtering/sorting";
      };

      files = lib.mkOption {
        type = types.listOf (types.submodule filesOpts);
        default = [ ];
        description = "List of files, that is to be made available to anyone who wants to solve the challenge";
      };

      lab = {
        networks = lib.mkOption {
          type = types.listOf (types.submodule networksOpts);
          default = [ ];
        };

        virtuals = lib.mkOption {
          type = types.listOf (types.submodule virtualsOpts);
          default = [ ];
          description = "A list of virtuals that should be used for this challenge, not required";
        };
      };
    };
  };
in module

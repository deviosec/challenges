{ config, lib, pkgs, ... }:

let
  inherit (lib) types;

  # saved for later to get all the nodes
  topconfig = config;

  nodeOptions = { name, config, ... }: {
    options = {
      name = lib.mkOption {
        type = types.str;
        default = name;
      };
      tag = lib.mkOption {
        type = types.str;
        default = "latest";
      };

      enabled = lib.mkOption {
        type = types.bool;
        default = true;
      };

      config = lib.mkOption {
        type = types.submoduleWith {
          specialArgs = {
              pkgs = pkgs;
              node = config;
              topconfig = topconfig;

              # specific node things
              nodes = lib.mapAttrs (name: value: value) topconfig.nodes;
          };

          modules = [
            # TODO(eyJhb): add a flag module, which can define where to put flags
            # and which users should have permissions to these flags
              ./node/build.nix
              ./node/basic.nix
              ./node/start.nix
              ./node/files.nix
              ./node/nginx.nix
              ./node/php.nix
              ./node/users.nix
              ./node/python-flask.nix
              ./node/mysql.nix
          ];
        };
      };
    };
  };
in {
  # TODO(eyJhb): add a name for the challenge + use this name for nodes names
  # when they are build, so that they can better be imported into docker
  options.nodes = lib.mkOption {
    type = types.attrsOf (types.submodule nodeOptions);
    default = {};
  };

  config = {
  };
}

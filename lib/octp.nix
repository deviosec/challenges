{ pkgs }:

pkgs.buildGoModule rec {
  pname = "octp";
  version = "unstable";

  src = pkgs.fetchFromGitLab {
    owner = "deviosec";
    repo = pname;
    rev = "2a63359a7c37c6e573f242c73ea409ba5b430de6";
    sha256 = "sha256-jD/NhqecGGFZzc8GrsHpk/gA4z7/uBvDTJ41yH8lyjY=";
  };

  vendorSha256 = "sha256-z4LoFgZ7LnX0xJyz4CA+agFDX9k9EaM/x27sNohguZk=";
}

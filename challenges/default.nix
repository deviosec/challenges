{ pkgs, lib }:

let
  nixDockerModules = ../lib;

  combineChallenges = challenges: pkgs.stdenv.mkDerivation {
    name = "challenges";
    phases = [ "buildPhase" ];
    buildPhase = let
      copyCmds = lib.mapAttrsToList (n: v: "ln -s ${v} $out/${n}") challenges;
    in ''
      mkdir -p $out
      ${lib.concatStringsSep "\n" copyCmds}
    '';
  };

  mkChallenge = challenge: extraConf: let
    resultConfig = if challenge ? result then challenge.result else challenge;
    testedConfig = if challenge ? tested then challenge.tested else challenge;
    result = import nixDockerModules { config = resultConfig; extraConfig = extraConf; };
    tested = import nixDockerModules { config = testedConfig; extraConfig = extraConf; };
  in {
    result = result.config.build.result;
    tested = tested.config.build.resulttested;
    config = result;
  };

  # make a top level challenge, by extracting (potential) extraConfig
  # and thereafter running mkChallenge on each entry in `challenges`
  mkTopLevelChallenge = topLevelChallenge: let
    extraConfig = if topLevelChallenge ? "extraConfig" then topLevelChallenge.extraConfig else {};

    # remove all override and overrideDerivation that comes from "pkgs.callPackage" in top-level
    filteredChallenges = lib.filterAttrs (n: _: if n == "override" || n == "overrideDerivation" then false else true) topLevelChallenge.challenges;
  in lib.mapAttrs (_: v: mkChallenge v extraConfig) filteredChallenges;

  # include all toplevel challenges
  challenges = let
    topLevelChallenges = import ./top-level.nix { pkgs = pkgs; };
  in lib.mapAttrs (_: v: mkTopLevelChallenge v) topLevelChallenges;

  # challenges WITH combined (result and tested)
  challengesWCombined = challengeSets: let

    mkCombined = name: challengeSet: let
      # combinedValue = 
        resultChallenges = lib.mapAttrs (_: v: v.result) challengeSet;
        testedChallenges = lib.mapAttrs (_: v: v.tested) challengeSet;

        combinedResultChallenges = combineChallenges resultChallenges;
        combinedTestedChallenges = combineChallenges testedChallenges;
    in [
      { name = name; value = challengeSet; }
      { name = "${name}-combined"; value = { result = combinedResultChallenges; tested = combinedTestedChallenges; }; }
    ];

    # stage 1 - make a list of challenge sets,
    # { set1 = { chall1 = ...; chall2 = ...; }; set2 = { ... }; }
    # ->
    # [
    #   [
    #     { name = "set1"; value = { ... } }
    #     { name = "set1-combined" value = { result = ...; tested = ...; } };
    #   ]
    #   [
    #     { name = "set2"; value = { ... } }
    #     { name = "set2-combined" value = { result = ...; tested = ...; } };
    #   ]
    # ]
    stage1 = lib.mapAttrsToList (n: v: mkCombined n v) challengeSets;

    # stage2 - flatten the list
    stage2 = lib.flatten stage1;

    # stage3 - make the list into attrs
    stage3 = builtins.listToAttrs stage2;
  in stage3;

in challengesWCombined challenges 
# in challenges

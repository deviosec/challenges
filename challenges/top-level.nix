{ pkgs, ... }:

{
  fbctf2019 = {
    challenges = pkgs.callPackage ./fbctf/fbctf2019 {};
    extraConfig = {
      overrides.challenge = x: x // {
        id = "fbctf2019-${x.id}";
        tags = x.tags ++ [ "fbctf2019" "fbctf" ];
      };
    };
  };

  aarhusctf2019 = {
    challenges = pkgs.callPackage ./aarhusctf/aarhusctf2019.nix {};
    extraConfig = {
      overrides.challenge = x: x // {
        id = "aarhusctf2019-${x.id}";
        tags = x.tags ++ [ "aarhusctf2019" "aarhusctf" ];
      };
    };
  };
}

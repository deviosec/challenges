{ pkgs, lib, ... }:

let
  # src path
  srcGitLab = pkgs.fetchFromGitLab {
    owner = "deviosec/challenge-repos";
    repo = "aarhusctf";
    rev = "88c819b3c60c9dd563e0ff41889c2223386921c1";
    sha256 = "sha256-mMFY1psUvxpMWfJCrv6/EorbL7sNS358VnZNqcnzOOc=";
  };
  src = srcGitLab + "/2019";

  challenges = {
    # web challenges
    awesome-calculator = { config, pkgs, ...}: {
      src = "${src}/web/awesome-calculator";

      challenge.config = rec {
        id = "awesome-calculator";
        title = "Awesome Calculator";
        description = "Checkout my new awesome web based calcualtor, it works great!";
        flags.MAIN = { flag = "CTF{Executing_Commands_Are_Awesome!}"; dynamic_env = true; };
        category = "web";
        tags = [ category ];
        difficulty = 25;

        hints = [
          { hint = "Maybe it is running some kind of external command?"; cost = 100; }
        ];

        lab.virtuals = [{
          ports.HTTP.port = 80;
          envs.FLAG = "##FLAG_MAIN##";
        }];
      };

      testing = {
        waitOnline.script = ''
          curl [$VIRTUAL_MAIN_IP]:$VIRTUAL_MAIN_PORT_HTTP | grep "Welcome to my awesome calculator!"
        '';

        script = ''
          curl [$VIRTUAL_MAIN_IP]:$VIRTUAL_MAIN_PORT_HTTP/calc?exp=2%2B2 | grep 'Your result is: <b>4'
          curl [$VIRTUAL_MAIN_IP]:$VIRTUAL_MAIN_PORT_HTTP/calc?exp=2%2B2%22%26%26cat%24%7BIFS%7D%2Fsrv%2Fapp%2Ffla%22%22g.txt%22 | grep '${config.challenge.config.flags.MAIN.flag}'
        '';
      };

      nodes.main.config = {
        files.files."/srv/app/flag.txt" = rec {
          file = pkgs.writeText "flag" config.challenge.config.flags.MAIN.flag;
          user = "app";
        };
        files.dirs."/srv/app" = rec {
          dir = src + "/web/awesome-calculator/root/srv/app";
          user = "app";
        };

        build.packages = with pkgs; [
          # reuirement
          bc # required for doing calculatinos
          bashInteractive # running with bc /bin/sh

          # for the users to play around with
          coreutils
        ];

        services.python-flask.enable = true;
      };
    };

    checklist = { config, ... }: {
      src = "${src}/web/checklist";

      challenge.config = rec {
        id = "checklist";
        title = "Checklist";
        description = "This server contains a flag in the file flag.txt colocated with the app. Can you retrieve it?";
        flags.MAIN = { flag = "CTF{SERVER_SIDE_TEMPLATE_INJECTION_IS_TOO_POWERFULL}"; dynamic_env = true; };
        category = "web";
        tags = [ category "injection" "template" ];
        difficulty = 25;

        hints = [
          { hint = "Seems like XSS is indeed possible on this side. Maybe Server Side Template Injection (SSTI) is as well?"; cost = 100; }
        ];

        lab.virtuals = [{
          ports.HTTP.port = 5000;
          envs.FLAG = "##FLAG_MAIN##";
        }];
      };

      nodes.main.config = {
        files = {
          files."/srv/app/flag.txt" = {
            file = pkgs.writeText "flag" config.challenge.config.flags.MAIN.flag;
            user = "app";
          };
          dirs."/srv/app" = {
            dir = src + "/web/checklist/src/srv/app";
            user = "app";
          };
        };

        services.python-flask.enable = true;
      };
    };

    flag-protocol = { config, ... }: {
      src = "${src}/web/flag-protocol";

      challenge.config = rec {
        id = "flag-protocol";
        title = "Flag Protocol";
        description = ''
            In addition to the normal [HTTP request methods](https://developer.mozilla.org/en-US/docs/Web/HTTP/Methods) there is one little known method. The method is FLAG.

            For instance a normal GET-request looks like this: `GET /index.html HTTP/1.1`

            Now your job is to send a FLAG-request instead - as you know, the protocol demands the server to respond with a flag.
        '';
        flags.MAIN = { flag = "CTF{GOTTA_LOVE_THAT_PROTOCOL}"; dynamic_env = true; };
        category = "web";
        tags = [ category ];
        difficulty = 25;

        hints = [
          { hint = "Try using a tool like curl, postman or firebug. You probably cannot do it from the browser."; cost = 100; }
        ];

        lab.virtuals = [{
          ports.HTTP.port = 80;
          envs.FLAG = "##FLAG_MAIN##";
        }];
      };

      testing = {
        waitOnline.script = "curl [$VIRTUAL_MAIN_IP]:$VIRTUAL_MAIN_PORT_HTTP | grep 'This should be a flag!'";
        script = "curl -X FLAG [$VIRTUAL_MAIN_IP]:$VIRTUAL_MAIN_PORT_HTTP | grep '${config.challenge.config.flags.MAIN.flag}' ";
      };

      nodes.main.config = { node, ... }: {
        files.dirs."/var/www/html" = rec {
          # patch with the correct flag
          dir = pkgs.stdenv.mkDerivation {
            name = "flag-protocol";
            src = src + "/web/flag-protocol/src/var/www/localhost/htdocs";
            phases = [ "unpackPhase" "patchPhase" "buildPhase" ];
            patchPhase = ''
              substituteInPlace index.php \
                --replace "#FLAG#" "${config.challenge.config.flags.MAIN.flag}"
           '';
            buildPhase = "mkdir -p $out && cp -a . $out";
          };

          group = "nginx";
        };

        services.nginx = {
          enable = true;
          virtualHosts.default.locations."/".settings =
            node.config.services.nginx.virtualHosts.default.locations.php-files.settings
            // { fastcgi_index = "index.php"; };
        };
        services.php.enable = true;
      };
    };

    navitas = { config, ... }: {
      src = "${src}/web/navitas";

      challenge.config = rec {
        id = "navitas";
        title = "Navitas";
        description = "The linux system has a user with a quite special username. Maybe you can find that?";
        flags.MAIN = { flag = "CTF{I_sh0Uld_n07_t4k3_P4thS_fR0M_t3H_U53R}"; dynamic_env = true; };
        category = "web";
        tags = [ category "lfi" "path-traversal" ];
        difficulty = 25;

        hints = [
          { hint = "This is called Local File Inclusion (LFI). However it seems the path has some kind of prefix."; cost = 100; }
        ];

        lab.virtuals = [{
          ports.HTTP.port = 80;
          envs.FLAG = "##FLAG_MAIN##";
        }];
      };

       testing = {
         waitOnline.script = "curl [$VIRTUAL_MAIN_IP]:$VIRTUAL_MAIN_PORT_HTTP | grep 'AU Navitas'";
         script = "curl [$VIRTUAL_MAIN_IP]:$VIRTUAL_MAIN_PORT_HTTP?page=../../../../../../../../../../../etc/passwd | grep '${config.challenge.config.flags.MAIN.flag}'";
       };

       nodes.main.config = {
         files.dirs."/var/www/html" = rec {
           dir = src + "/web/navitas/src/var/www/localhost/htdocs";
           group = "nginx";
         };

         # plant the flag in /etc/passwd
         users.users.monty.comment = config.challenge.config.flags.MAIN.flag;

         services.nginx.enable = true;
         services.php.enable = true;
       };
     };

    registration = { config, ... }: {
      src = "${src}/web/registration";

      challenge.config = rec {
        id = "registration";
        title = "Registration";
        description = "Just register for a flag - if they have more left at least.";
        flags.MAIN = { flag = "CTF{CLIENT_SIDE_VALIDATION_IS_NOT_ENOUGH}"; dynamic_env = true; };
        category = "web";
        tags = [ category "input-validation" ];
        difficulty = 25;

        hints = [
          { hint = "Maybe validation only happens on the client-side? You might be able to manipulate the request using Developer Tools, curl or even postman."; cost = 100; }
        ];

        lab.virtuals = [{
          ports.HTTP.port = 80;
          envs.FLAG = "##FLAG_MAIN##";
        }];
      };

      nodes.main.config = {
        files.dirs."/var/www/html" = rec {
          # patch with the correct flag
          dir = pkgs.stdenv.mkDerivation {
            name = "registration";
            src = src + "/web/registration/src/var/www/localhost/htdocs";
            phases = [ "unpackPhase" "patchPhase" "buildPhase" ];
            patchPhase = ''
              substituteInPlace index.php \
                --replace "#FLAG#" "${config.challenge.config.flags.MAIN.flag}"
            '';
            buildPhase = "mkdir -p $out && cp -a . $out";
          };

          group = "nginx";
        };
        services.nginx.enable = true;
        services.php.enable = true;
      };
    };

    admin-cookie = { config, ... }: {
      src = "${src}/web/admin-cookie";
      challenge.config = rec {
        id = "do-you-want-a-cookie";
        title = "Do you want a cookie?";
        description = "Can you bypass the access control and get the flag?";
        flags.MAIN.flag = "CTF{never_trust_a_client}";
        category = "web";
        tags = [ category ];
        difficulty = 10;

        lab.virtuals = [{
          ports.HTTP.port = 80;
        }];
      };

      testing = {
        waitOnline.script = "curl [$VIRTUAL_MAIN_IP]:$VIRTUAL_MAIN_PORT_HTTP | grep 'Flag Vault'";
        script = "curl --cookie 'admin=yes' [$VIRTUAL_MAIN_IP]:$VIRTUAL_MAIN_PORT_HTTP | grep '${config.challenge.config.flags.MAIN.flag}'";
      };

      nodes.main.config = {
        files.dirs."/var/www/html" = rec {
          dir = src + "/web/admin-cookie/src/www";
          group = "nginx";
        };
        services.nginx.enable = true;
        services.php.enable = true;
      };
    };

    magic-hash = { config, ... }: {
      src = "${src}/web/magic-hash";
      challenge.config = rec {
        id = "magic-bypass";
        title = "Magic";
        description = "Can you bypass the access control and get the flag?";
        flags.MAIN.flag = "CTF{maybe_we_should_use_the_triple_equal_sign_next_time}";
        hints = [ { hint = "PHP type juggling can be dangerous"; cost = 100; } ];
        category = "web";
        tags = [ category ];
        difficulty = 10;

        lab.virtuals = [{
          ports.HTTP.port = 80;
        }];
      };

      testing = {
        waitOnline.script = "curl [$VIRTUAL_MAIN_IP]:$VIRTUAL_MAIN_PORT_HTTP | grep 'Magic'";
        script = " curl -X POST -d 'pass=240610708' [$VIRTUAL_MAIN_IP]:$VIRTUAL_MAIN_PORT_HTTP | grep '${config.challenge.config.flags.MAIN.flag}'";
      };

      nodes.main.config = { pkgs, lib, ... }: {
        files.files."/var/flag.txt" = rec {
          file = pkgs.writeText "magic-hash-flag" config.challenge.config.flags.MAIN.flag;
          group = "nginx";
        };
        files.dirs."/var/www/html" = rec {
          dir = src + "/web/magic-hash/www";
          group = "nginx";
        };
        services.nginx.enable = true;
        services.php.enable = true;
      };
    };

    password-timing-attack = {
      src = "${src}/web/password-timing-attack";
      challenge.config = rec {
        id = "clock-is-ticking";
        title = "The Clock is Ticking";
        description = ''
          You probably need a stopwatch for this challenge.
          Note: A bit of intelligent brute-forcing is required for this one,
          but please do not run the top 10 million password list against it.
          Servers have feelings too.
        '';
        flags.MAIN.flag = "CTF{gotta_go_fast}";
        category = "web";
        tags = [ category ];
        difficulty = 60;
        lab.virtuals = [{
          ports.HTTP.port = 80;
        }];
      };

      nodes.main.config = { pkgs, lib, ... }: {
        files.dirs."/var/www/html" = rec {
          dir = src + "/web/password-timing-attack/src/www";
          group = "nginx";
        };
        services.nginx.enable = true;
        services.php.enable = true;
      };
    };

    sql-boolean-blind = {
      src = "${src}/web/sql-boolean-blind";
      challenge.config = rec {
        id = "leakdb";
        title = "LeakDB";
        description = "Can you find the flag stored on this site? Note: A bit of intelligent brute-forcing is required for this one, but please be nice to the poor server. It is doing its very best.";
        flags.MAIN.flag = "CTF{oh_did_you_just_dump_me?}";
        hints = [ { hint = "You're blind, but you can still see differences on the site - right? Maybe write a script for that."; cost = 100; } ];
        category = "web";
        tags = [ category ];
        difficulty = 40;
        lab.virtuals = [{
          ports.HTTP.port = 80;
        }];
      };
      nodes.main.config = { pkgs, lib, ... }: {
        files.files."/var/db/data.db" = rec {
          file = src + "/web/sql-boolean-blind/data/data.db";
          group = "nginx";
        };
        files.dirs."/var/www/html" = rec {
          dir = src + "/web/sql-boolean-blind/www";
          group = "nginx";
        };
        services.nginx.enable = true;
        services.php.enable = true;
      };
    };

    user-agent = {
      src = "${src}/web/user-agent";
      challenge.config = rec {
        id = "secret-agent";
        title = "Secret Agent";
        description = "Can you infiltrate this site and find the flag?";
        flags.MAIN.flag = "CTF{4ny_1npu7_c4n_b3_m4n1pul473d!}";
        category = "web";
        tags = [ category ];
        difficulty = 10;
        lab.virtuals = [{
          ports.HTTP.port = 80;
        }];
      };
      nodes.main.config = { pkgs, lib, ... }: {
        files.dirs."/var/www/html" = rec {
          dir = src + "/web/user-agent/www";
          group = "nginx";
        };
        services.nginx.enable = true;
        services.php.enable = true;
      };
    };

    # misc 
    repeated-encoding = {
      src = "${src}/misc/repeated-encoding";
      challenge.config = rec {
        id = "say-what-again";
        title = "Say what again";
        description = "Encoding is not encryption. How hard can this challenge be?";
        flags.MAIN.flag = "CTF{i_double_dare_you}";
        category = "misc";
        tags = [ category ];
        difficulty = 10;
        files = [ { filename = "flag.txt"; } ];
      };
    };

    braille = {
      src = "${src}/misc/braille";
      challenge.config = rec {
        id = "see-this-coming";
        title = "You didn't see this coming!";
        description = "Can you read these characters?";
        flags.MAIN.flag = "CTF{cant_touch_this}";
        category = "misc";
        tags = [ category ];
        difficulty = 60;
        files = [ { filename = "flag.png"; } ];
      };
    };

    base64-lowercase = {
      src = "${src}/misc/base64-lowercase";
      challenge.config = rec {
        id = "base63-5";
        title = "Base63.5";
        description = "The file contains an encoded flag, but something went wrong with the encoding. Can you decode it?";
        flags.MAIN.flag = "CTF{that_is_just_annoying}";
        category = "misc";
        tags = [ category ];
        difficulty = 20;
        files = [ { filename = "flag.txt"; } ];
      };
    };

    # # crypto
    pierre = {
      src = "${src}/crypto/pierre";

      challenge.config = rec {
        id = "pierre";
        title = "Pierre";
        description = "Poor Prerre's private key has been lost!";
        flags.MAIN = { flag = "CTF{R4nd0M_pr1m4s_4_RSA_Pl34s3!}"; dynamic_build = true; };
        category = "crypto";
        tags = [ category ];
        difficulty = 50;

        hints = [
          {
            hint = "There's a certain famous mathematician whose first name is Pierre. Maybe some of his work can be used for this challenge?";
            cost = 100;
          }
        ];

        files = [
          { filename = "output/pierre.public.key"; }
          { filename = "output/flag.enc"; }
        ];
      };
    };


    # TODO(eyJhb): Fix this
    # "3des-hmac" = let
    #   config = rec {
    #     id = "3des-hmac";
    #     title = "3DES-HMAC";
    #     description = "This challenge is written in Rust. So it is automatically secure, right?";
    #     flags.MAIN = { flag = "CTF{D0_n0T_t311_m3_tH1s_w45_n3iTheR_3DES_n0r_HMAC}"; dynamic_env = true; };
    #     category = "crypto";
    #     tags = [ category ];
    #     difficulty = 100;
    #     files = [ { filename = "dist.zip"; } ];
    #     lab.virtuals = [{
    #       ports.HTTP = 12001;
    #     }];
    #   };

    #   extraCmds = config: ''
    #     # setup flag
    #     rm flag.txt
    #     echo '${config.flags.MAIN.flag}' > flag.txt

    #     # package files
    #     ${pkgs.zip}/bin/zip -r dist.zip Cargo.lock Cargo.toml src/ static/ templates/
    #   '';
    # in simpleBuild2 { defaultConfig = config; path = "${src}/crypto/3des-hmac"; extraCmds = extraCmds; };

    "2-for-1" = { config, ... }: {
      src = "${src}/crypto/2-for-1";

      testing = {
        waitOnline.script = ''
          echo "" | nc -N $VIRTUAL_MAIN_IP $VIRTUAL_MAIN_PORT_NC | grep "Hello, and welcome to the"
        '';

        script = let
          pythonEnv = pkgs.python3.withPackages (ps: with ps; [
            pycrypto
            pwntools
          ]);
        in ''
          ${pythonEnv}/bin/python ${config.src}/solution.py $VIRTUAL_MAIN_IP $VIRTUAL_MAIN_PORT_NC | grep '${config.challenge.config.flags.MAIN.flag}'
        '';
      };

      challenge.config = rec {
        id = "2-for-1";
        title = "2-for-1";
        description = ''
          The server implements an "Oblivious Transfer" protocol based on the Paillier cryptosystem.

          The server has two messages:

            m0 = k XOR flag
            m1 = k

          The client can send an encrypted bit Enc(b) and obtain either m0 or m1, by having the server compute Enc(m0 + b(m1 - m0)).
        '';
        flags.MAIN = { flag = "CTF{b3w4R3_0f_m4L1c10u5_R3ce1V3r5}"; dynamic_env = true; };
        category = "crypto";
        tags = [ category ];
        difficulty = 100;
        files = [ { filename = "helpers.py"; } ];
        lab.virtuals = [{
          ports.NC.port = 12001;
        }];
      };

      nodes.main.config = { node, pkgs, lib, ... }: {
        files.files."/srv/app/flag.txt" = rec {
          file = pkgs.writeText "flag" config.challenge.config.flags.MAIN.flag;
          user = "app";
        };
        files.files."app" = rec {
          file = src + "/crypto/2-for-1/src/server.py";
          destination = "/srv/app/server.py";
          user = "app";
        };

        cwd = "/srv/app";
        users.users.app = {};

        services.start.app = let
          pythonEnv = pkgs.python3.withPackages(ps: with ps; [ pycrypto ]);
        in lib.dag.entryAnywhere {
          command = "${pythonEnv}/bin/python ${node.config.files.files.app.destination}";
          user = "app";
        };
      };
    };

    # TODO(eyJhb): solution does not seem to work
    aimless-aes = { config, pkgs, ... }: {
      src = "${src}/crypto/aimless-aes";
      challenge.config = rec {
        id = "aimless-aes";
        title = "Aimless AES";
        description = ''
          You are given an AES-128 ECB encrypted file. AES is (as far as we
          know) unbroken, but luckily for you, you are also given a set of
          partial AES round keys. Decrypt the file to get the flag :)
        '';
        flags.MAIN.flag = "CTF{_r0und_k3ys}";
        category = "crypto";
        tags = [ category ];
        difficulty = 100;
        files = [ { filename = "enc.bin"; } { filename = "key.txt"; } ];
      };

      build.extraCmds = let
        pythonEnv = pkgs.python3.withPackages (ps: with ps; [
          pycryptodome
        ]);
      in ''
        rm -f enc.bin key.txt
        sed -i 's|CTF{_r0und_k3ys}|${config.challenge.config.flags.MAIN.flag}|g' challenge.py
        ${pythonEnv}/bin/python challenge.py
      '';
    };

    extreme-xor = { config, pkgs, ... }: {
      src = "${src}/crypto/extreme-xor";
      challenge.config = rec {
        id = "extreme-xor";
        title = "Extreme XOR";
        description = ''
          The file is encrypted using a repeating XOR cipher. Can you break
          the encryption and find the flag? Free hint: The plaintext is in
          English
        '';
        flags.MAIN.flag = "CTF{4round_4round_4round_1T_g03s}";
        category = "crypto";
        tags = [ category ];
        difficulty = 100;
        files = [ { filename = "enc.bin"; } ];
      };

      build.extraCmds = ''
        rm enc.bin
        sed -i 's|CTF{4round_4round_4round_1T_g03s}|${config.challenge.config.flags.MAIN.flag}|g' input.txt
        ${pkgs.python3}/bin/python challenge.py
      '';
    };

    # TODO(eyJhb): server runs, but gives weird error
    free-flag = { config, ... }: {
      src = "${src}/crypto/freeflag";
      challenge.config = rec {
        id = "freeflag";
        title = "FreeFlag";
        description = ''
          Go to the website at the website to obtain an encrypted flag.
          To recover the flag, simply go to /getflag and input the encrypted flag.

          (TODO: finish implementation)
        '';
        flags.MAIN = { flag = "CTF{uH_0H_4rr3s_4r3_d4ng3R0u5_L0L}"; dynamic_env = true; };
        category = "crypto";
        tags = [ category ];
        difficulty = 100;
        lab.virtuals = [{
          ports.HTTP.port = 5000;
        }];
      };

      nodes.main.config = { node, pkgs, ...}: {
        files.files."/srv/app/flag.txt" = rec {
          file = pkgs.writeText "flag" config.challenge.config.flags.MAIN.flag;
          user = node.config.services.python-flask.user;
        };
        files.files.${node.config.services.python-flask.apppath} = {
          file = src + "/crypto/freeflag/src/server.py";
          user = node.config.services.python-flask.user;
        };

        cwd = "/srv/app";

        services.python-flask = {
          enable = true;
          package = pkgs.python3.withPackages(ps: with ps; [ pycrypto flask ]);
        };
      };
    };

    ottt = { config, ... }: {
      src = "${src}/crypto/ottt";
      challenge.config = rec {
        id = "ottt";
        title = "OTTT";
        description = ''
          The server runs a "One Time Truth Table" MPC
          protocol.

          The goal is to guess which random number (between 0 and 9) the
          server uses, 50 times in a row.

          Good luck!
        '';
        flags.MAIN = { flag = "CTF{N0_M4c5_m4k3z_MPC_1ns3Cur3}"; dynamic_env = true; };
        category = "crypto";
        tags = [ category ];
        difficulty = 100;
        lab.virtuals = [{
          ports.MAIN.port = 12002;
        }];
      };

      nodes.main.config = { node, pkgs, lib, ... }: {
        files.files."app" = rec {
          file = pkgs.stdenv.mkDerivation {
            name = "ottt-patch";
            src = src + "/crypto/ottt/src";
            phases = [ "unpackPhase" "patchPhase" "buildPhase" ];
            patchPhase = ''
              substituteInPlace server.py \
                --replace "{{FLAG}}" "${config.challenge.config.flags.MAIN.flag}"
           '';
            buildPhase = "cp server.py $out";
          };

          destination = "/srv/app/server.py";
          user = "app";
        };

        cwd = "/srv/app";
        users.users.app = {};

        services.start.app = let
          pythonEnv = pkgs.python3.withPackages(ps: with ps; [ pycrypto ]);
        in lib.dag.entryAnywhere {
          command = "${pythonEnv}/bin/python ${node.config.files.files.app.destination}";
          user = "app";
        };
      };
    };

    ottt-v2 = { config, ... }: {
      src = "${src}/crypto/ottt-v2";
      challenge.config = rec {
        id = "ottt-v2";
        title = "OTTT V2";
        description = ''
          Welp. We've added message authentication codes, so now it shouldn't
          be possible to cheat anymore!

          Goal is the same as before.

          NOTE: This is a continuation of OTTT
        '';
        flags.MAIN = { flag = "CTF{sh1tty_r4nd0mn355_br34k5_s3cur1ty}"; dynamic_env = true; };
        category = "crypto";
        tags = [ category ];
        difficulty = 100;
        lab.virtuals = [{
          ports.MAIN.port = 12003;
        }];
      };

      nodes.main.config = { node, pkgs, lib, ... }: {
        files.dirs."/srv/app" = rec {
          dir = pkgs.stdenv.mkDerivation {
            name = "otttv2-patch";
            src = src + "/crypto/ottt-v2/src";
            phases = [ "unpackPhase" "patchPhase" "buildPhase" ];
            patchPhase = ''
              substituteInPlace server.py \
                --replace "{{FLAG}}" "${config.challenge.config.flags.MAIN.flag}"
           '';
            buildPhase = "mkdir -p $out && cp -a mac.py server.py $out";
          };
          user = "app";
        };

        cwd = "/srv/app";
        users.users.app = {};

        services.start.app = let
          pythonEnv = pkgs.python3.withPackages(ps: with ps; [ pycrypto ]);
        in lib.dag.entryAnywhere {
          command = "${pythonEnv}/bin/python /srv/app/server.py";
          user = "app";
        };
      };
    };

    rsa-small-e = { config, pkgs, ... }: {
      src = "${src}/crypto/rsa-small-e";
      challenge.config = rec {
        id = "arguably-rsa";
        title = "Arguably RSA";
        description = "The file contains an RSA public key and a ciphertext. Can you break the encryption and get the flag?";
        flags.MAIN.flag = "CTF{WhatIsThisDevilStuff?}";
        category = "crypto";
        tags = [ category ];
        difficulty = 100;
        files = [ { filename = "key.txt"; } ];
      };

      
      build.extraCmds = let
        pythonEnv = pkgs.python3.withPackages (ps: with ps; [
          pycrypto
        ]);
      in ''
        sed -i 's|CTF{r3memb3r_2_p4d_RSA_:)}|${config.challenge.config.flags.MAIN.flag}|g' challenge.py
        ${pythonEnv}/bin/python challenge.py
      '';
    };

    # # todo(eyJhb) requires first repeated-xor,
    # # they are not in the same category?? fix this?
    repeated-xor-2 = { pkgs, ... }: {
      src = "${src}/crypto/repeated-xor-2";
      challenge.config = rec {
        id = "repeated-xor-2";
        title = "Repeated XOR 2: Electric Boogaloo";
        description = "Here's another repeated XOR encryption. The key is 10 bytes and the plaintext is a 500x281 image";
        flags.MAIN.flag = "CTF{B3w4RE_PTXT_kn0wL3dg3}";
        category = "crypto";
        tags = [ category ];
        difficulty = 100;
        files = [ { filename = "flag.bin"; } ];
      };
      
      build.extraCmds = "${pkgs.python3}/bin/python challenge.py";
    };

    single-character-xor = { config, pkgs, ... }: {
      src = "${src}/crypto/single-character-xor";
      challenge.config = rec {
        id = "exciting-xor";
        title = "Exciting XOR";
        description = "The file is encrypted using a simple XOR cipher. Can you break the encryption and get the flag?";
        flags.MAIN.flag = "CTF{H4rdly_4_s3cur3_c1ph3r}";
        category = "crypto";
        tags = [ category ];
        difficulty = 100;
        files = [ { filename = "enc.bin"; } ];
      };

      build.extraCmds = ''
        sed -i 's|CTF{H4rdly_4_s3cur3_c1ph3r}|${config.challenge.config.flags.MAIN.flag}|g' challenge.py
        ${pkgs.python3}/bin/python challenge.py
      '';
    };

    gamebox = { config, ... }: {
      src = "${src}/crypto/gamebox";
      challenge.config = rec {
        id = "gamebox";
        title = "GameBox";
        description = "Would you like to play a game with me?";
        flags.MAIN.flag = "CTF{I_l1K3_t0_pl4y_r4ciNg_6aM35_0n_tH3_pl4Y5t4t1On}";
        category = "crypto";
        tags = [ category ];
        difficulty = 100;
        lab.virtuals = [{
          ports.GAMEBOX.port = 10000;
          ports.SHOP.port = 9999;
        }];
        files = [ { filename = "ecc.py"; } { filename = "shop.py"; } { filename = "gamebox.py"; } ];
      };

      nodes.main.config = { pkgs, lib, ... }: {
        files.files."/home/gamebox/flag.txt" = rec {
          file = pkgs.writeText "flag.txt" config.challenge.config.flags.MAIN.flag;
          user = "gamebox";
        };
        files.dirs."/home/gamebox" = rec {
          dir = pkgs.stdenv.mkDerivation {
            name = "gamebox-files";
            src = src + "/crypto/gamebox";
            phases = [ "unpackPhase" "buildPhase" ];
            buildPhase = "mkdir -p $out && cp -a ecc.py gamebox.py shop.py secret.py $out";
          };
          user = "gamebox";
        };

        cwd = "/home/gamebox";
        users.users.gamebox = {};

        services.start = let
          pythonEnv = pkgs.python3.withPackages(ps: with ps; [ gmpy2 pyaes ]);
        in {
          shop = lib.dag.entryBefore [ "gamebox" ] {
            command = "${pythonEnv}/bin/python /home/gamebox/shop.py";
            user = "gamebox";
          };
          gamebox = lib.dag.entryAfter [ "shop" ] {
            command = "${pythonEnv}/bin/python /home/gamebox/gamebox.py";
            user = "gamebox";
          };
        };
      };
    };

    indistinguishable = { config, ... }: {
      src = "${src}/crypto/indistinguishable";
      challenge.config = rec {
        id = "indistinguishable";
        title = "Indistinguishable";
        description = "I see just random data.  Can you help me out?";
        flags.MAIN.flag = "CTF{RC4_1s_R3411y_Cr4ppy_4_3ncrypt10n}";
        category = "crypto";
        tags = [ category ];
        difficulty = 100;
        lab.virtuals = [{
          ports.MAIN.port = 1337;
        }];
        files = [ { filename = "indistinguishable.py"; } ];
      };

      nodes.main.config = { pkgs, lib, ... }: {
        files.files."/srv/app/flag.txt" = rec {
          file = pkgs.writeText "flag.txt" config.challenge.config.flags.MAIN.flag;
          user = "app";
        };
        files.files."/srv/app/indistinguishable.py" = rec {
          file = src + "/crypto/indistinguishable/indistinguishable.py";
          user = "app";
        };

        cwd = "/srv/app";
        users.users.app = {};

        services.start = let
          pythonEnv = pkgs.python3.withPackages(ps: with ps; [ pycryptodome ]);
        in {
          shop = lib.dag.entryAnywhere {
            command = ''${pkgs.socat}/bin/socat TCP-LISTEN:1337,reuseaddr,fork EXEC:"${pythonEnv}/bin/python -u /srv/app/indistinguishable.py"'';
            user = "app";
          };
        };
      };
    };

    # # forensics
    dns = {
      src = "${src}/forensics/dns";

      challenge.config = rec {
        id = "hello";
        title = "Hello?";
        description = "...";
        flags.MAIN = { flag = "CTF{p1ng_p0ng_p0rt5_w_DNS}"; };
        category = "forensics";
        tags = [ category ];
        difficulty = 50;

        hints = [
          { hint = "DNS is usually sent to port 53"; cost = 100; }
        ];

        files = [
          { filename = "junk.pcap"; }
        ];
      };
    };

    image-metadata = {
      src = "${src}/forensics/image-metadata";
      challenge.config = rec {
        id = "smiley";
        title = ":-)";
        description = ":-)";
        flags.MAIN.flag = "CTF{1m4g3s_4r3_fuLL_0f_s3cr3t5}";
        category = "forensics";
        tags = [ category ];
        difficulty = 100;
        files = [ { filename = "hey.png"; } ];
      };
    };

    usb-capture= {
      src = "${src}/forensics/usb-capture";
      challenge.config = rec {
        id = "serious-business";
        title = "Urgent Serious Business";
        description = "Capturing network traffic is old school. The cool kids are capturing USB traffic now.";
        flags.MAIN.flag = "CTF{type_like_nobody's_watching}";
        category = "forensics";
        tags = [ category ];
        difficulty = 100;
        files = [ { filename = "usb.pcapng"; } ];
      };
    };

    # # introduction
    # # todo(eyJhb) change category ??
    flag-format = { config, ... }: {
      src = "${src}/introduction/flag-format";
      challenge.config = rec {
        id = "flag-format";
        title = "Baby's first flag";
        description = ''
          Woah! Capture the Flag is all about what we all love - finding flags!

          All flags follows the same format unless otherwise written in a challenge description. Note that all flags are case sensitive. ctf{something} and CTF{something} are not the same.

          NOTE: We try to follow this as much as possible, but there may be errors where this is not the case.
          Please report such things to us!
        '';
        flags.MAIN.flag = "CTF{GIVE_ME_MORE_FLAGS!}";
        category = "misc";
        tags = [ category ];
        difficulty = 1;

        files = [ { filename = "flag.txt"; } ];

      };

      build.extraCmds = "echo '${config.challenge.config.flags.MAIN.flag}' > flag.txt";
    };

    # sql-introduction = let
    #   config = rec {
    #     id = "sql-introduction";
    #     title = "SQL Introduction";
    #     description = "Here is the introduction challenge SQL-injections. Try to play around with it. In the bottom you can see the query we run on the server, based upon your input. Can you make it return true?";
    #     flags.MAIN = { flag = "CTF{simple_old_and_effective}"; dynamic_env = true; };
    #     hints = [ { hint = "Try using quotes and see if that helps"; cost = 100; } ];
    #     category = "misc";
    #     tags = [ category ];
    #     difficulty = 1;
    #     lab.virtuals = [{
    #       ports.HTTP = 80;
    #     }];
    #   };
    # in simpleBuild config "${src}/introduction/sql";

    xss-introduction = { config, ... }: {
      src = "${src}/introduction/xss";
      challenge.config = rec {
        id = "xss-introduction";
        title = "XSS Introduction";
        description = ''
          Here is the introduction challenge to Cross-side Scripting (XSS). Try to use javascript to display the cookie.
        
          Redirects are usefull too, but not needed here.
        '';
        flags.MAIN = { flag = "CTF{XSS_CAN_BE_DONE_USING_REDIRECTS_AS_WELL}"; dynamic_env = true; };
        category = "misc";
        tags = [ category ];
        difficulty = 1;
        lab.virtuals = [{
          ports.HTTP.port = 80;
        }];
      };

      nodes.main.config = { pkgs, lib, ... }: {
        files.dirs."/var/www/html" = {
          dir = pkgs.stdenv.mkDerivation {
            name = "xss-introduction";
            src = src + "/introduction/xss/src/var/www/localhost/htdocs";
            phases = [ "unpackPhase" "patchPhase" "buildPhase" ];
            patchPhase = ''
              substituteInPlace functions.js \
                --replace "#FLAG#" "${config.challenge.config.flags.MAIN.flag}"
           '';
            buildPhase = "mkdir -p $out && cp -a . $out";
          };
          group = "nginx";
        };

        services.nginx.enable = true;
        services.php.enable = true;
      };
    };

    # # pwn
    random-password = let
      pkg = pkgs.gccMultiStdenv.mkDerivation {
        name = "random-password-bin";
        src = src + "/pwn/random-password";
        hardeningDisable = [ "fortify" ];
        installPhase = "install -Dm755 random_password $out/bin/random_password ";
      };
    in { config, ... }: {
      src = "${src}/pwn/random-password";

      challenge.config = rec {
        id = "random-password";
        title = "Random Password";
        description = "See if you guess the random password ;)";
        flags.MAIN.flag = "CTF{Y0u_h4ve_tO_B3_r3a11y_g00d_4t_gue55In6}";
        category = "pwn";
        tags = [ category ];
        difficulty = 50;
        lab.virtuals = [{
          ports.NC.port = 4747;
        }];
        files = [ { filename = "random_password"; } ];
      };

      build.extraCmds = "cp ${pkg}/bin/random_password $out";

      testing = {
        waitOnline.script = ''
          echo "John Doe" | nc -N $VIRTUAL_MAIN_IP $VIRTUAL_MAIN_PORT_NC | grep "Hello John Doe"
        '';

        script =''
          ${pkgs.python3}/bin/python3 ${config.src}/exploit.py --host $VIRTUAL_MAIN_IP --port $VIRTUAL_MAIN_PORT_NC | grep '${config.challenge.config.flags.MAIN.flag}'
        '';
      };

      nodes.main.config = { pkgs, lib, ... }: {
        files = {
          files."/home/user/random_password" = {
            file = "${pkg}/bin/random_password";
            user = "user";
            permissions = "ug=rx,o=";
          };
          files."/home/user/flag.txt" = {
            file = pkgs.writeText "flag.txt" (config.challenge.config.flags.MAIN.flag + "\n");
            user = "user";
          };
        };

        build.packages = with pkgs; [
          # so the user can spawn /bin/sh
          bashInteractive
          coreutils
        ];

        users.users.user = {};
        services.start = {
          app = lib.dag.entryAnywhere {
            command = ''${pkgs.socat}/bin/socat TCP-LISTEN:4747,reuseaddr,fork EXEC:"/home/user/random_password"'';
            user = "user";
          };
        };
      };
    };

    # TODO(eyJhb): fixup the files, as they are currently not included
    book-manager = {
      src = "${src}/pwn/bookmanager";

      challenge.config = rec {
        id = "book-manager";
        title = "Book Manager 4.7";
        description = ''
            a great place to store you favourite books. Books that I would put on my
            shelf would include e.g. the Silmarillion, 1984 if you like it
            realistic, The Time Machine, or Foundations of Cryptography.
        '';
        flags.MAIN = { flag = "CTF{YoU_411_5h0uLd_r3aD_mOr3_b00k5_(Or_p4P3R5_:)}"; dynamic_env = true; };
        category = "pwn";
        tags = [ category ];
        difficulty = 50;

        files = [
          { filename = "output/bookmanager"; }
          { filename = "src-generator/libc-2.24.so"; }
        ];

        lab.virtuals = [{
          ports.NC.port = 4747;
          envs.FLAG = "##FLAG_MAIN##";
        }];
      };

      nodes.main.config = { config, pkgs, lib, ... }: {
        files.files."/home/user/bookmanager" = let
          pkg = pkgs.stdenv.mkDerivation {
            name = "book-manager";
            src = src + "/pwn/bookmanager/src-generator";
            installPhase = "install -Dm755 bookmanager $out/bin/bookmanager";
          };
        in {
          file = "${pkg}/bin/bookmanager";
          user = "user";
          permissions = "ug=rx,o=";
        };

        users.users.user = {};

        services.start = {
          app = lib.dag.entryAnywhere {
            command = ''${pkgs.socat}/bin/socat TCP-LISTEN:4747,reuseaddr,fork EXEC:"/home/user/bookmanager"'';
            user = "user";
          };
        };
      };
    };

    broken-register = {
      src = "${src}/pwn/broken-register";
      challenge.config = rec {
        id = "broken-register";
        title = "Broken Register";
        description = "I found this old machine in my attic.  See if it still works.";
        flags.MAIN.flag = "CTF{0Oops_that_oO0verflo0oOwed}";
        category = "pwn";
        tags = [ category ];
        difficulty = 100;
        lab.virtuals = [{
          ports.MAIN.port = 4701;
        }];
        files = [ { filename = "broken_register"; } ];
      };

      nodes.main.config = { pkgs, lib, ... }: {
        files.files."/home/user/broken_register" = let
          pkg = pkgs.multiStdenv.mkDerivation {
            name = "broken-register";
            src = src + "/pwn/broken-register";
            installPhase = "install -Dm755 broken_register $out/bin/broken_register";
          };
        in {
          file = "${pkg}/bin/broken_register";
          user = "user";
          permissions = "ug=rx,o=";
        };

        users.users.user = {};

        services.start = {
          app = lib.dag.entryAnywhere {
            command = ''${pkgs.socat}/bin/socat TCP-LISTEN:4701,reuseaddr,fork EXEC:"/home/user/broken_register"'';
            user = "user";
          };
        };
      };
    };
    # todo(eyJhb) add reversing
  };

in challenges

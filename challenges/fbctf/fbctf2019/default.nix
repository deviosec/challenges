{ pkgs, lib, ... }:

let
  srcPatch = patches: pkgs.stdenvNoCC.mkDerivation {
    name = "fbctf2019";

    src = pkgs.fetchFromGitHub {
        owner = "fbsamples";
        repo = "fbctf-2019-challenges";
        rev = "4353c2ce588cf097ac6ca9bcf7b943a99742ac75";
        sha256 = "sha256-DB3YkAmw8FGuV+F2bU60zP+6C1+aWQiY7yZKtglOYkE=";
    };

    phases = [
      "unpackPhase"
      "patchPhase"
      "installPhase"
    ];

    installPhase = ''
      mkdir $out
      cp -a . $out
    '';

    patches = [
      # products manager
      ./patches/web-products-manager.patch

      # secret note keeper
      ./patches/web-secret-note-keeper-admin-bot.patch
      ./patches/web-secret-note-keeper-main-sql.patch
      ./patches/web-secret-note-keeper-solution.patch
      ./patches/web-pdfme.patch
    ] ++ patches;
  };

  srcNoPatch = srcPatch [];

  challenges = {
    # web
    rceservice = import ./web-rceservice.nix { src = srcNoPatch; pkgs = pkgs; lib = lib; };
    products-manager = import ./web-products-manager.nix { src = srcNoPatch; pkgs = pkgs; lib = lib; };
    secret-note-keeper = import ./web-secret-note-keeper.nix { src = srcPatch; pkgs = pkgs; lib = lib; };
    pdfme = import ./web-pdfme.nix { src = srcNoPatch; pkgs = pkgs; lib = lib; };
  };
in challenges


{ src, pkgs, lib, ... }:

let
  srcDir = src + "/web/pdfme/";

  # get older version of nixpkgs, for older libreoffice
  oldpkgs = import (pkgs.fetchFromGitHub {
      name = "nixpkgs-old-libre";
      owner = "NixOS";
      repo = "nixpkgs";
      # libreoffice 5.4.1.2
      rev = "b1273f24539a9b5f50d3086b4a9f4fc3bb6c0a50";
      sha256 = "sha256-WzUhA/mKkzxS7p1+gs64bLhF75XD57SPtiynKcTMCuc=";
  }) {};                                                                           

  oldlibreoffice = oldpkgs.libreoffice-fresh;
in { config, lib, ... }: {
  challenge.config = rec {
    id = "pdfme";
    title = "pdfme";
    description = "We setup this PDF conversion service for public use, hopefully it is safe.";
    flags.MAIN.flag = "fb{wh0_7h0u6h7_l1br30ff1c3_c4n_b3_u53ful}";
    category = "web";
    tags = [ category ];
    difficulty = 25;

    lab.virtuals = [{
      ports.HTTP.port = 80;
    }];
  };

  testing = {
    waitOnline.script = "curl [$VIRTUAL_MAIN_IP]:$VIRTUAL_MAIN_PORT_HTTP | grep 'PDF manager'";
    script = ''
      curl -F "file=@${srcDir}/poc.fods" [$VIRTUAL_MAIN_IP]:$VIRTUAL_MAIN_PORT_HTTP | ${pkgs.poppler_utils}/bin/pdftotext - - | grep "${config.challenge.config.flags.MAIN.flag}"
    '';
  };

  nodes.main.config = { pkgs, lib, ... }: let
    appuser = "libreoffice_admin";
  in {
    users.users."${appuser}"= {
      home = "/home/${appuser}";
    };

    files.files = {
      "/var/app/app.py" = {
        file = srcDir + "/app/app.py";
        user = appuser;
      };

      "/var/app/templates/index.html" = {
        file = srcDir + "/app/templates/index.html";
        user = appuser;
      };

      "/home/${appuser}/flag" = {
        file = pkgs.writeText "flag" config.challenge.config.flags.MAIN.flag;
        user = appuser;
      };
    };

    build.packages = with pkgs; [
      oldlibreoffice

      # required by libreoffice
      gnugrep
      gnused
    ];

    services.start.app = let
      pythonEnv = pkgs.python3.withPackages(ps: with ps; [
        flask
      ]);
    in lib.dag.entryAnywhere {
      command = "${pythonEnv}/bin/python /var/app/app.py";
      user = appuser;
    };
  };
}

{ src, pkgs, lib, ... }:

let
  srcDir = src + "/web/rceservice/";
in { config, lib, ... }: {
  challenge.config = rec {
    id = "rceservice";
    title = "rceservice";
    description = "We created this web interface to run commands on our servers, but since we haven't figured out how to secure it yet we only let you run 'ls'";
    flags.MAIN.flag = "fb{pr3g_M@tcH_m@K3s_m3_w@Nt_t0_cry!!1!!1!}";
    category = "web";
    tags = [ category ];
    difficulty = 50;

    files = [ { filename = "dist.zip"; } ];

    lab.virtuals = [{
      ports.HTTP.port = 80;
    }];
  };

  build.output.paths."dist.zip" = lib.mkDist "dist" (srcDir + "/src") (_: _: true);

  testing = {
    waitOnline.script = "curl [$VIRTUAL_MAIN_IP]:$VIRTUAL_MAIN_PORT_HTTP | grep 'Enter command as JSON'";
    script = let
      cmdJSON = ''{
        "cmd": "/bin/cat /home/rceservice/flag"
      }'';

    in ''
      curl -d cmd='${cmdJSON}' [$VIRTUAL_MAIN_IP]:$VIRTUAL_MAIN_PORT_HTTP | grep '${config.challenge.config.flags.MAIN.flag}'
    '';
  };

  nodes.main.config = { pkgs, lib, ... }: {
    build.packages = with pkgs; [ coreutils bashInteractive ];

    services.nginx.enable = true;
    services.php.enable = true;

    files.files = {
      "/home/rceservice/flag" = {
        file = pkgs.writeText "rceservice-flag" config.challenge.config.flags.MAIN.flag;
        permissions = "ugo=r";
      };
      "/home/rceservice/jail/ls" = {
        file = "${pkgs.coreutils}/bin/ls";
        permissions = "ugo=rx";
      };

      "/var/www/html/index.php" = {
        file = srcDir + "/src/index.php";
        group = "nginx";
      };
    };

    users.users.rceservice = {};
  };
}

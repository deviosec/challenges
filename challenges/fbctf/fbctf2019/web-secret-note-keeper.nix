{ src, pkgs, lib, ... }:

let
  srcResult = (src []) + "/web/secret_note_keeper";
  srcTested = (src [ ./patches/web-secret-note-keeper-test-bot.patch ]) + "/web/secret_note_keeper";

  mkChallenge = srcDir: { config, lib, ... }: {
    challenge.config = rec {
      id = "secret-note-keeper";
      title = "Secret Note Keeper";
      description = "Find the secret note that contains the fl4g! (note: flag is all lowercase)";
      flags.MAIN.flag = "fb{cr055_s173_l34|<5_4r4_c00ool!!}";
      category = "web";
      tags = [ category ];
      difficulty = 25;
  
      lab = {
        virtuals = [
          {
            name = "web";
            ports.HTTP.port = 80;
          }
          {
            name = "bot";
            envs = {
              "OCTP_PUBLIC_ADDRESS" = "";
              "OCTP_PUBLIC_PORT_WEB_HTTP" = "";
            };
          }
        ];
  
        networks = [
          {
            name = "public-network";
            connections = [ "bot" ];
          }
        ];
      };
    };

    build.output.paths."solution.py" = srcDir + "/solution.py";
  
    testing = {
      waitOnline.script = "curl [$VIRTUAL_WEB_IP]:$VIRTUAL_WEB_PORT_HTTP | grep 'The most secure note keeper'";
      timeout = 5*60;
      script = let
        pythonEnv = pkgs.python3.withPackages (ps: with ps; [ requests flask ]);
      in ''
        ( ${pythonEnv}/bin/python ${srcDir}/solution.py http://$LOCAL_IPv4:$VIRTUAL_WEB_PORT_HTTP $LOCAL_IPv4 8080 || true ) | grep '${config.challenge.config.flags.MAIN.flag}'
      '';
    };
  
    nodes.bot.config = { pkgs, lib, node, ... }: {
      users.users.bot.home = "/home/bot";
      cwd = "/var/app";
  
      files.dirs."bot-dir" = {
        dir = srcDir + "/adminbot/bot";
        destination = "/var/app";
        user = "bot";
      };
  
      build.packages = with pkgs; [
        firefox
        geckodriver
      ];
  
      services.start.app = let
        pythonEnv = pkgs.python3.withPackages(ps: with ps; [
          pymysql
          selenium
        ]);
      in lib.dag.entryAnywhere {
        command = "${pythonEnv}/bin/python ${node.config.files.dirs."bot-dir".destination}/admin_bot.py";
        user = "bot";
      };
    };
  
    nodes.web.config = { pkgs, lib, node, ... }: {
      users.users.app = {};
  
      files.dirs."app-dir" = {
        dir = srcDir + "/web/app/app";
        destination = "/var/app";
        user = "app";
      };
  
      services.start.app = let
        pythonEnv = pkgs.python3.withPackages(ps: with ps; [
          flask
          flask_sqlalchemy
          flask_login
          mysqlclient
        ]);
      in lib.dag.entryAnywhere {
        command = "${pythonEnv}/bin/python ${node.config.files.dirs."app-dir".destination}/main.py";
        user = "app";
      };
  
      services.mysql = {
        enable = true;
        bind = "0.0.0.0";
  
        users.chal1 = {
          username = "chal1";
          password = "getbvllnbunnhriunuglchillki";
          host = "%";
          access."default" = {
            host = "%";
            database = "notes";
          };
        };
        users.chal1-localhost = {
          username = "chal1";
          password = "getbvllnbunnhriunuglchillki";
          access."default" = {
            database = "notes";
          };
        };
        databases."notes".sqlFiles = [
          (srcDir + "/web/app/notes_dump.sql")
        ];
      };
    };
  };

in {
  result = mkChallenge srcResult;
  tested = mkChallenge srcTested;
}

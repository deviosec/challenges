{ src, pkgs, lib, ... }:

let
  srcDir = src + "/web/products-manager/";
in { config, lib, ... }: {
  challenge.config = rec {
    id = "products-manager";
    title = "Products Manager";
    description = "This challenge is a database storage where users can create and view products (protected by secrets). There are already 5 products in the database, one of them has the flag in its description.";
    flags.MAIN.flag = "fb{4774ck1n9_5q1_w17h0u7_1nj3c710n_15_4m421n9_:)}";
    category = "web";
    tags = [ category ];
    difficulty = 25;

    files = [ { filename = "dist.zip"; } ];

    lab.virtuals = [{
      ports.HTTP.port = 80;
    }];
  };

  build.output.paths."dist.zip" = lib.mkDist "dist" (srcDir + "/src") (n: v: if lib.hasInfix "config.php" n then false else true);

  testing = {
    waitOnline.script = "curl [$VIRTUAL_MAIN_IP]:$VIRTUAL_MAIN_PORT_HTTP | grep 'Products Manager'";
    script = let
      pythonEnv = pkgs.python3.withPackages (ps: with ps; [ requests ]);
    in "${pythonEnv}/bin/python ${srcDir}/solve.py [$VIRTUAL_MAIN_IP] $VIRTUAL_MAIN_PORT_HTTP | grep '${config.challenge.config.flags.MAIN.flag}'";
  };

  nodes.main.config = { pkgs, lib, ... }: {
    files.dirs."/var/www/html" = {
      dir = srcDir + "/src";
      group = "nginx";
    };

    services.nginx.enable = true;
    services.php.enable = true;

    services.mysql = {
      enable = true;
      package = pkgs.mysql80;
      initializeType = "mysql";

      users.products_manager = {
        password = "je6H92Gagf91Ha";
        access."default".database = "products_manager";
      };
      databases."products_manager" = {
        sqlFiles = [
          (srcDir + "/mysql/schema.sql")
        ];
      };
    };
  };
}
